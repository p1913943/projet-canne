/***********************************************************************
 * Module:  Film.java
 * Author:  p1913943
 * Purpose: Defines the Class Film
 ***********************************************************************/

package Planning;

import java.util.*;

public class Film {

    
   private int idFilm;
   private java.lang.String nomFilm;
   private int duree;
   private String categorie;
   private int temoin;
   
   public Film(int idFilm, String nomFilm, int duree, String categorie)
   {
       this.idFilm = idFilm;
       this.nomFilm = nomFilm;
       this.duree = duree;
       this.categorie = categorie;
       this.temoin = 0;
   }
   
   public int getDuree() {
        return duree;
    }

    public String getCategorie() {
        return categorie;
    }

    public int getTemoin() {
        return temoin;
    }

    public void setTemoin(int temoin) {
        this.temoin = temoin;
    }

    public String getNomFilm() {
        return nomFilm;
    }
    
    
    
    
}