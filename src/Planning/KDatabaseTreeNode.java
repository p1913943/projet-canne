package Planning;


import java.net.URL;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.tree.DefaultMutableTreeNode;

public class KDatabaseTreeNode extends DefaultMutableTreeNode {
    
    private static final long serialVersionUID = -2126135007480343273L;

    
    public KDatabaseTreeNode( Connection connection ) throws SQLException {
        if ( connection == null ) return;
        String text = connection.getCatalog() + " database";
        String url = "images/database.jpg";
        this.setUserObject( new UserObject( text, url ) );
        
        this.add( new TablesTreeNode( connection ) );
        // You can do the same for views and stored procedures
    }

    
    
    public static class TablesTreeNode extends DefaultMutableTreeNode {

        private static final long serialVersionUID = 5512631571960795765L;

        public TablesTreeNode( Connection connection ) throws SQLException {
            String url = "images/tables.jpg";
            this.setUserObject( new UserObject( "Tables", url ) );

            Statement stmt = null;
        
            stmt = connection.createStatement();
            String query = "SHOW TABLES";

            ResultSet rset = null;
            rset = stmt.executeQuery(query);
                          
            while( rset.next() ) {
                if (rset.getString(1).equals("Categorie") || rset.getString(1).equals("Film") || rset.getString(1).equals("Salle"))
                {
                    this.add( new TableTreeNode( rset.getString(1), connection ) );
                }                
            }            
        }
    }

    public static class TableTreeNode extends DefaultMutableTreeNode {

        private static final long serialVersionUID = 2744534418719443466L;

        public TableTreeNode( String tableName, Connection connection ) throws SQLException {
            
            String url = "images/table.jpg";
            this.setUserObject( new UserObject( tableName, url ) );
            
            Statement stmt = null;
        
            stmt = connection.createStatement();
            
            String query = null;
            if (tableName.equals("Categorie"))
            {
                query = "select nomCat from " + tableName;
            }
            
            if (tableName.equals("Film"))
            {
                query = "select nomFilm from " + tableName;
            }
            
            if (tableName.equals("Salle"))
            {
                query = "select nomSalle from " + tableName;
            }
            

            ResultSet rset = null;
            rset = stmt.executeQuery(query);
            while (rset.next())
            {    
                if (rset.getString(1) != null)
                {
                    if (!rset.getString(1).equals("null"))
                    {
                        this.add( new ContenuTreeNode(rset.getString(1))); 
                    }
                }
                                                     
            }
        }
        
    }
    
 
    
    public static class ContenuTreeNode extends DefaultMutableTreeNode {

        private static final long serialVersionUID = 2744534418719443466L;

        public ContenuTreeNode( String lineName ) {
            String url = "images/ligne.png";
            this.setUserObject( new UserObject( lineName, url ) );

            
        }
        
        @Override public boolean isLeaf() {
            return true;
        }
        
    }

    public static class UserObject {
        private String text;
        private String iconUrl;
        
        public UserObject( String text, String iconUrl ) {
            this.text = text;
            this.iconUrl = iconUrl;
        }
        
        public String getText() {
            return text;
        }
        
        public String getIconUrl() {
            return iconUrl;
        }
    }
}
