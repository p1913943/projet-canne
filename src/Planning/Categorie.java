/***********************************************************************
 * Module:  Categorie.java
 * Author:  p1913943
 * Purpose: Defines the Class Categorie
 ***********************************************************************/

package Planning;

import java.util.*;

public class Categorie {
    
    private int idCat;
    private String nomCat;
    private int jrDebut;
    private int jrFin;
    private List<Integer> typeCreneau;
    private String salleLendemain;
    private int nbRepresentation;
    
    public Categorie(int idCat, String nomCat, int jrDebut, int jrFin, String catCreneau, String salleLendemain) {
        
        this.idCat = idCat;
        this.nomCat = nomCat;
        this.jrDebut = jrDebut;
        this.jrFin = jrFin;
        List<Integer> tmpCreneau = new ArrayList<>();
        if (catCreneau != null)
        {
            String[] tmp = catCreneau.split(",");
            for (int i = 0; i < tmp.length; i++)
            {           
                int rsl = Integer.parseInt(tmp[i]);
                tmpCreneau.add(rsl);
            }
        }
        this.typeCreneau = tmpCreneau;
        this.salleLendemain = salleLendemain;

        
    }

    public int getJrDebut() {
        return jrDebut;
    }

    public int getJrFin() {
        return jrFin;
    }

    public String getNomCat() {
        return nomCat;
    }

    public List<Integer> getTypeCreneau() {
        return typeCreneau;
    }

    public int getNbRepresentation() {
        return nbRepresentation;
    }

    public String getSalleLendemain() {
        return salleLendemain;
    }
    
    
    
    

}