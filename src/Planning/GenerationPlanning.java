/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Planning;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;

/**
 *
 * @author mathiseynaud
 */
public class GenerationPlanning {
    
    private static List<Categorie> listeCategorie;
    private static List<Film> listeFilm;
    private static List<Salle> listeSalle;
    
    private static int tmpPose = 50;
    
    
       public static void generer(String d) throws SQLException {
            
                listeCategorie = null;
                listeFilm = null;
                listeSalle = null;
                requette();
                asignJournee(d);
                
                for (int i = 0; i < listeSalle.size(); i++)
                {
                    
                    Salle tmpSalle = listeSalle.get(i);

                    
                    
                    for (int j = 0; j < tmpSalle.getListJournee().size(); j++)
                    {
                        Journee tmpJournee = tmpSalle.getListJournee().get(j);
                        
                        for (int k = 0; k < listeFilm.size(); k++)
                        {
                            Film tmpFilm = listeFilm.get(k);
                            
                            
                            if (testCat(tmpFilm.getCategorie(),tmpSalle.getNomCat1(),tmpSalle.getNomCat2()) && testDate(tmpJournee.getIdJournee(),getCat(tmpFilm.getCategorie()).getJrDebut(),getCat(tmpFilm.getCategorie()).getJrFin()) && testHeure(tmpJournee.getTopH(), getCat(tmpFilm.getCategorie()).getTypeCreneau()))
                            {
                                prodCreneau(tmpFilm, tmpJournee, k);                                                                                                                                                                                                                                        
                            }
                        }
                    }
                }
       }
       
    public static void requette() throws SQLException {
        DataSource dataSource =  MonMariaDbDataSource.getMdbDataSource();
        Connection connection = dataSource.getConnection();
        
        CategorieDAO categorieDao = new CategorieDAO(connection);
        listeCategorie = categorieDao.getCategorie();
              
        FilmDAO filmDao = new FilmDAO(connection);
        listeFilm = filmDao.getFilm();
        
        SalleDAO salleDao = new SalleDAO(connection);
        listeSalle = salleDao.getSalle();
        
        connection.close();
       }
       
       public static void asignJournee(String d) {
           for (int i = 0; i < listeSalle.size(); i++)
            {
                listeSalle.get(i).createJournee(d);
            }
       }
       
       public static Categorie getCat(String nomCat)
       {
           Categorie categorie = null;
           for (int i = 0; i < listeCategorie.size(); i++)
           {
               
               if (listeCategorie.get(i).getNomCat().equals(nomCat))
               {
                   categorie = listeCategorie.get(i);
               }
           }
           return categorie;
           
       }
       
       public static boolean testCat(String catFilm, String catSalle1, String catSalle2)
       {
           if (catFilm.equals(catSalle1) || catFilm.equals(catSalle2))
           {
               return true;
           }
           return false;
       }
       
       public static boolean testDate(int idJournee, int jrDebut, int jrFin)
       {
           if (idJournee >= jrDebut && idJournee <= jrFin)
           {
               return true;
           }
           return false;
       }
       
       public static boolean testHeure(int jTopH, List<Integer> typeCreneau)
       {
           if (jTopH == 510)
           {
               for (int i = 0; i < typeCreneau.size(); i++)
               {
                   if (typeCreneau.get(i) == 1)
                   {
                       return true;
                   }
               }
           }
           
           if (jTopH > 510 && jTopH <= 840)
           {
               for (int i = 0; i < typeCreneau.size(); i++)
               {
                   if (typeCreneau.get(i) == 2)
                   {
                       return true;
                   }
               }
               
           }
           
           if (jTopH > 840 && jTopH <= 960)
           {
               for (int i = 0; i < typeCreneau.size(); i++)
               {
                   if (typeCreneau.get(i) == 3)
                   {
                       return true;
                   }
               }
               
           }
           
           if (jTopH > 960 && jTopH <= 1140)
           {
               for (int i = 0; i < typeCreneau.size(); i++)
               {
                   if (typeCreneau.get(i) == 4)
                   {
                       return true;
                   }
               }
               
           }
           
           if (jTopH > 1140 && jTopH <= 1320)
           {
               for (int i = 0; i < typeCreneau.size(); i++)
               {
                   if (typeCreneau.get(i) == 5)
                   {
                       return true;
                   }
               }
               
           }
           
           return false;
       }
       
       public static void Afficher()
       {
           for (int i = 0; i < listeSalle.size(); i++)
           {
               System.out.println(listeSalle.get(i).getNomSalle()+"\n");
               
               for (int j = 0; j < listeSalle.get(i).getListJournee().size(); j++)
               {
                   System.out.println(" "+listeSalle.get(i).getListJournee().get(j).getDate()+"\n");
                   for (int k = 0; k < listeSalle.get(i).getListJournee().get(j).getCreneau().size(); k++)
                   {
                       System.out.println("  "+listeSalle.get(i).getListJournee().get(j).getCreneau().get(k).getHeure() / 60 +","+ listeSalle.get(i).getListJournee().get(j).getCreneau().get(k).getHeure() % 60);
                       System.out.println("  "+listeSalle.get(i).getListJournee().get(j).getCreneau().get(k).getFilm().getNomFilm()+"\n");
                   }
               }
           }
       }

    public static List<Film> getListeFilm() {
        return listeFilm;
    }
       
       
    public static Salle getSalle(String nomSalle)
    {
        Salle salle = null;
        for (int i = 0; i < listeSalle.size(); i++)
        {
            if (listeSalle.get(i).getNomSalle().equals(nomSalle))
            {
               salle = listeSalle.get(i);
            }
        }
        return salle;
    }
    
    public static void prodCreneau(Film film, Journee journee, int k)
    {
        
        int idJournee = journee.getIdJournee();
        if (film.getCategorie().equals("LM"))
        {
            if (film.getDuree() > 150 && journee.getCreneauDispo() >= 2)
            {
                for (int n = 0; n < 2; n++)
                {
                    if (testHeure(journee.getTopH(), getCat(film.getCategorie()).getTypeCreneau()) && testDate(journee.getIdJournee(), getCat(film.getCategorie()).getJrDebut(), getCat(film.getCategorie()).getJrFin()))
                    {
                        journee.addCreneau(film, journee.getTopH());
                        journee.setTopH(journee.getTopH() + film.getDuree() + tmpPose);
                        journee.setCreneauDispo(journee.getCreneauDispo() - 1);
                        
                        if (getCat(film.getCategorie()).getSalleLendemain() != null)
                        {
                            Salle tmpSalle = getSalle(getCat(film.getCategorie()).getSalleLendemain());

                            if (idJournee + 1 < tmpSalle.getListJournee().size())
                            {       
                                Journee tmpJournee = tmpSalle.getListJournee().get(idJournee);
                                tmpJournee.addCreneau(film, tmpJournee.getTopH());
                                tmpJournee.setTopH(tmpJournee.getTopH() + film.getDuree() + tmpPose);
                            }

                        }
                    }
                                            
                                    
                }
                
                
                listeFilm.remove(k);
                k--;
            }
            
            else 
            {
                if (journee.getCreneauDispo() >= 3)
                {
                    for (int n = 0; n < 3; n++)
                    {
                        if (testHeure(journee.getTopH(), getCat(film.getCategorie()).getTypeCreneau()) && testDate(journee.getIdJournee(), getCat(film.getCategorie()).getJrDebut(), getCat(film.getCategorie()).getJrFin()))
                        {
                            journee.addCreneau(film, journee.getTopH());
                            journee.setTopH(journee.getTopH() + film.getDuree() + tmpPose);
                            journee.setCreneauDispo(journee.getCreneauDispo() - 1);
                            
                            if (getCat(film.getCategorie()).getSalleLendemain() != null)
                            {
                                Salle tmpSalle = getSalle(getCat(film.getCategorie()).getSalleLendemain());

                                if (idJournee + 1 < tmpSalle.getListJournee().size())
                                {       
                                    Journee tmpJournee = tmpSalle.getListJournee().get(idJournee);
                                    tmpJournee.addCreneau(film, tmpJournee.getTopH());
                                    tmpJournee.setTopH(tmpJournee.getTopH() + film.getDuree() + tmpPose);
                                }

                            }

                        }


                    }
                    
                    listeFilm.remove(k);
                    k--;
                }
                
            }
            
                                    
        }
        
        if (film.getCategorie().equals("UCR"))
        {
            if (journee.getCreneauDispo() >= 2)
            {
                for (int n = 0; n < 2; n++)
                {
                    if (testHeure(journee.getTopH(), getCat(film.getCategorie()).getTypeCreneau()) && testDate(journee.getIdJournee(), getCat(film.getCategorie()).getJrDebut(), getCat(film.getCategorie()).getJrFin()))
                    {
                        journee.addCreneau(film, journee.getTopH());
                        journee.setTopH(journee.getTopH() + film.getDuree() + tmpPose);
                        journee.setCreneauDispo(journee.getCreneauDispo() - 1);
                        
                        if (getCat(film.getCategorie()).getSalleLendemain() != null)
                        {
                            Salle tmpSalle = getSalle(getCat(film.getCategorie()).getSalleLendemain());

                            if (idJournee + 1 < tmpSalle.getListJournee().size())
                            {                  
                                Journee tmpJournee = tmpSalle.getListJournee().get(idJournee);
                                tmpJournee.addCreneau(film, tmpJournee.getTopH());
                                tmpJournee.setTopH(tmpJournee.getTopH() + film.getDuree() + tmpPose);
                            }

                        }
                    }                       
                }
                listeFilm.remove(k);
                k--;
            }
            
            
        }
        
        
        
        if (film.getCategorie().equals("CM"))
        {   

            if (testHeure(journee.getTopH(), getCat(film.getCategorie()).getTypeCreneau()) && testDate(journee.getIdJournee(), getCat(film.getCategorie()).getJrDebut(), getCat(film.getCategorie()).getJrFin()))
            {
                journee.addCreneau(film, journee.getTopH());
                journee.setTopH(journee.getTopH() + film.getDuree());
            } 
            
                                                  
        }
        
        if (film.getCategorie().equals("HC") && journee.getCreneauDispo() >= 1)
        {
            
            if (testHeure(journee.getTopH(), getCat(film.getCategorie()).getTypeCreneau()) && testDate(journee.getIdJournee(), getCat(film.getCategorie()).getJrDebut(), getCat(film.getCategorie()).getJrFin()))
            {
                journee.addCreneau(film, journee.getTopH());
                journee.setTopH(journee.getTopH() + film.getDuree());
                listeFilm.remove(k);
                k--;
                journee.setCreneauDispo(journee.getCreneauDispo() - 1);
            }  
            
        }
            
    }
        
    
    
    public static List<Film> getListCM()
    {
        List<Film> listeCM = new ArrayList<>();
        for (int i = 0; i < listeFilm.size(); i++)
        {
            if (listeFilm.get(i).getCategorie().equals("CM"))
            {
                listeCM.add(listeFilm.get(i));
                listeFilm.remove(i);
            }
        }
        return listeCM;
    }
       
    public static Journee jourSuiv(Journee journee)
    {
        Journee tmpJournee = null;
        for (int i = 0; i < journee.getSalle().getListJournee().size(); i++)
        {
            if (journee.getSalle().getListJournee().get(i).getIdJournee() == journee.getIdJournee() + 1)
            {
                tmpJournee = journee.getSalle().getListJournee().get(i);
            }
        }        
        return tmpJournee;
    }    

    public static List<Salle> getListeSalle() {
        return listeSalle;
    }
    
    public static boolean ajoutCreneau(Salle salle, String Date, Film film, int heure)            
    {
        int cpt = 0;
        Journee journee = null;
        for (int i = 0; i < salle.getListJournee().size(); i++)
        {
            if (salle.getListJournee().get(i).getDate().equals(Date))
            {
                journee = salle.getListJournee().get(i);
            }
        }
        
        for (int i = 0; i < journee.getCreneau().size(); i++)
        {
            
            int hDebut = journee.getCreneau().get(i).getHeure();
            int hFin = journee.getCreneau().get(i).getFilm().getDuree() + hDebut;            
            
            if (!(heure < hDebut && heure + film.getDuree() < hDebut || heure > hFin && heure + film.getDuree() > hFin))
            {
                System.out.println("erreur");
                cpt = -1;
            }
        }
        
        if (cpt == 0)
        {            
            journee.addCreneau(film, heure);
            return true;
        }
        else
        {
            return false;
        }
    }

    public static List<Categorie> getListeCategorie() {
        return listeCategorie;
    }
    
    public static Film getFilm(String nomFilm) throws SQLException
    {
        Film film = null;
        DataSource dataSource =  MonMariaDbDataSource.getMdbDataSource();
        Connection connection = dataSource.getConnection();
        
        FilmDAO filmDao = new FilmDAO(connection);
        List<Film> listeFilm = filmDao.getFilm();
        
        for (int i = 0; i < listeFilm.size(); i++)
        {
            if (listeFilm.get(i).getNomFilm().equals(nomFilm))
            {
               film = listeFilm.get(i);
            }
        }
        return film;
    }
    
    
}
