/***********************************************************************
 * Module:  Journee.java
 * Author:  p1913943
 * Purpose: Defines the Class Journee
 ***********************************************************************/

package Planning;

import java.awt.BorderLayout;
import java.util.*;

public class Journee {
   private int idJournee;
   private String date;  
   private List<Creneau> creneau = new ArrayList<>();
   private int TopH;
   private Salle salle;
   private int creneauDispo;
   
   public Journee(String date, int idJournee, Salle salle) {
       this.date = date;
       this.idJournee = idJournee;
       this.TopH = 510;
       idJournee++;
       this.salle = salle;
       this.creneauDispo = calculCreneauDispo(salle);
       
   }
   
   public int calculCreneauDispo(Salle salle)
   {
       Categorie cat1 = GenerationPlanning.getCat(salle.getNomCat1());
       Categorie cat2 = GenerationPlanning.getCat(salle.getNomCat2());
       int cpt = 0;
       
       if (cat2 != null && cat1 != null)
       {
            for (int i = 0; i < 5; i++)
            {
            boolean test = false;
            for (int j = 0; j < 5; j++)
            {
                if (cat1.getTypeCreneau().size() > j)
                {
                    if (cat1.getTypeCreneau().get(j) == i + 1)
                    {
                        test = true;
                    }
                }
                
                if (cat2.getTypeCreneau().size() > j)
                {
                    if (cat2.getTypeCreneau().get(j) == i + 1)
                    {
                        test = true;
                    }
                } 
                
            }
            cpt++;
           
            }
       return cpt;
       }
       
       if (cat1 != null)
       {
           for (int i = 0; i < 5; i++)
           {
               if (cat1.getTypeCreneau().size() > i)
               {
                   if (cat1.getTypeCreneau().get(i) == i + 1)
                {
                    cpt++;
                }
               }
               
           }
       }
       else 
       {
           cpt = 0;
       }
       return cpt;
   }
   
   public void addCreneau(Film film, int heure)
   {
       this.creneau.add(new Creneau(film, heure));
   }

    public int getTopH() {
        return TopH;
    }

    public void setTopH(int TopH) {
        this.TopH = TopH;
    }

    public int getIdJournee() {
        return idJournee;
    }

    public List<Creneau> getCreneau() {
        return creneau;
    }

    public String getDate() {
        return date;
    }

    public Salle getSalle() {
        return salle;
    }

    public int getCreneauDispo() {
        return creneauDispo;
    }

    public void setCreneauDispo(int creneauDispo) {
        this.creneauDispo = creneauDispo;
    }
    
    
    
    
}