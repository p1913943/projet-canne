/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Planning;

import java.sql.*;
import java.util.List;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;

/**
 *
 * @author mathiseynaud
 */
public class FilmDAO {
    
    Connection connexionBD;
    
    public FilmDAO (Connection conn) {
        this.connexionBD = conn;
    }
    
    public List<Film> getFilm() {
        ResultSet rset = null;
        Statement stmt = null;
        List<Film> listeFilm = null;
        String query = "SELECT idFilm, nomFilm, duree, nomCat FROM Film INNER JOIN Categorie ON Film.idCat = Categorie.idCat;";
        Film art;
        try {
            
            stmt = connexionBD.createStatement();
            listeFilm = new ArrayList<>();
            rset = stmt.executeQuery(query);

            while (rset.next()) {
           
            art = new Film(rset.getInt(1), rset.getString(2), rset.getInt(3), rset.getString(4));
            listeFilm.add(art);
            }

        } catch (SQLException ex) {
            Logger.getLogger( FilmDAO.class.getName() ).log(Level.SEVERE, null, ex);
        }
    return listeFilm;
    }
    
}
