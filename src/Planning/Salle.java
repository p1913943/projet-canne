/***********************************************************************
 * Module:  Salle.java
 * Author:  p1913943
 * Purpose: Defines the Class Salle
 ***********************************************************************/

package Planning;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class Salle {
   private int idSalle;
   private java.lang.String nomSalle;
   private String nomCat1;
   private String nomCat2;
   private List<Journee> listeJournee = new ArrayList<>();
   private int constructeurJour = 0;
   
      
   public Salle(int idSalle, String nomSalle, String nomCat1, String nomCat2) {
       this.idSalle = idSalle;
       this.nomSalle = nomSalle;
       this.nomCat1 = nomCat1;
       this.nomCat2 = nomCat2;
       
       
   }
   
   public void createJournee(String d) {
       Calendar date = getDate(d);
       DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
       
       for (int i = 1; i <= 12; i++)
       {
           listeJournee.add(new Journee(format.format(date.getTime()), i, this));
           date.add(Calendar.DATE, 1);
       }
   }
   
   public List<Journee> getListJournee() {
       return this.listeJournee;
   }
   
   public String getNomCat1() {
        return nomCat1;
    }

    public String getNomCat2() {
        return nomCat2;
    }

    public String getNomSalle() {
        return nomSalle;
    }
    
    public Calendar getDate(String date)
    {
        DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        Calendar tmpDate = Calendar.getInstance();
        
        while (true)
        {
            if (format.format(tmpDate.getTime()).equals(date))
            {              
                return tmpDate;
            }
            tmpDate.add(Calendar.DATE, 1);
        }   
    }
    
    

}