/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Planning;

import java.sql.*;
import java.util.List;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;

/**
 *
 * @author mathiseynaud
 */
public class SalleDAO {
    
    Connection connexionBD;
    
    public SalleDAO (Connection conn) {
        this.connexionBD = conn;
    }
    
    public List<Salle> getSalle() {
        ResultSet rset = null;
        ResultSet rset2 = null;
        
        Statement stmt = null;
        List<Salle> listeSalle = null;
        String query = "SELECT idSalle, nomSalle, nomCat, idCat2 FROM Salle INNER JOIN Categorie ON Salle.idCat = Categorie.idCat";
        Salle art;
        try {
            stmt = connexionBD.createStatement();                       
            listeSalle = new ArrayList<>();
            rset = stmt.executeQuery(query);
            

            while (rset.next()) {
            
                query = "select nomCat from Categorie where idCat = " + rset.getInt(4);
                rset2 = stmt.executeQuery(query);
                rset2.next();
                String rslt = rset2.getString(1);
            
                art = new Salle(rset.getInt(1), rset.getString(2), rset.getString(3), rslt);
                listeSalle.add(art);
            }

        } catch (SQLException ex) {
            Logger.getLogger( FilmDAO.class.getName() ).log(Level.SEVERE, null, ex);
        } 
    return listeSalle;
    }
    
}
