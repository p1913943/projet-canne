/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Planning;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.sql.DataSource;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.JTree;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;

/**
 *
 * @author mathiseynaud
 */

public class Form extends JFrame {
    
    private JPanel contentPane = (JPanel) this.getContentPane();
    private JButton generer = new JButton("Générer");
    private JButton modifier = new JButton("Modifier le planning");
    private JMenuBar menuBar = new JMenuBar();
    private static JTabbedPane planning = new JTabbedPane();
    private JPanel panelDroit = new JPanel();
    private JButton ajouter = new JButton("Ajouter un film");
    private JButton supprimer = new JButton("Supprimer un film");
    private JButton modifSeance = new JButton("Échanger deux séances");
    private KDatabaseTree bd = new KDatabaseTree();
    private JScrollPane bdScrollPane = new JScrollPane( bd );
    private JSplitPane splitPainGauche = new JSplitPane( JSplitPane.HORIZONTAL_SPLIT, bdScrollPane, planning );
    private static JTextArea textArea = new JTextArea();
    private static JScrollPane scrollArea = new JScrollPane( textArea );
    private JButton validAjouter = new JButton("valider");
    private static JTextField textDuree = new JTextField();
    private static JTextField textNomAuteur = new JTextField();
    private static JTextField textNomFilm = new JTextField();
    private static JComboBox boxHeure = new JComboBox();
    private static JComboBox boxMinute = new JComboBox();
    private static JComboBox dureeHeure = new JComboBox();
    private static JComboBox dureeMinute = new JComboBox();
    private static JComboBox boxSalle = new JComboBox();
    private static JComboBox boxdate = new JComboBox();
    private static JComboBox boxCategorie = new JComboBox();
    private static JComboBox boxCategorie2 = new JComboBox();
    private JButton validerDate = new JButton("ok");
    private static JComboBox boxFilm = new JComboBox();
    private JButton validerFilm = new JButton("ok");
    private static JComboBox boxHeureFilm = new JComboBox();
    private JButton validerHeure = new JButton("ok");
    private JButton validerSuppr = new JButton("Supprimer");
    private JPanel panelSalle = new JPanel();
    private JButton validerSalle = new JButton("ok");
    private JPanel panelFilm = new JPanel();
    private JPanel paneldate = new JPanel();
    private JPanel panelHeure = new JPanel();
    private JDialog boite = new JDialog(this);
    private JPanel boiteContent = (JPanel) boite.getContentPane();
    private JButton validerSeance1 = new JButton("Valider la première séance");
    private String salleCreneau1;
    private String dateCreneau1;
    private String filmCreneau1;
    private String heureCreneau1;
    private JButton echanger = new JButton("échanger");
    private JButton modifierBd = new JButton("Modifier la base de donnée");
    private JDialog dialogueBd = new JDialog(this);
    private JPanel boiteBd = (JPanel) dialogueBd.getContentPane();
    private JButton modifFilm = new JButton("Film");
    private JButton ajoutFilm = new JButton("ajouter un film");
    private JButton supprFilm = new JButton("supprimer un film");
    private JButton modifCategorie = new JButton("Categorie");
    private JButton ajoutCategorie = new JButton("ajouter une catégorie");
    private JButton supprCategorie = new JButton("supprimer une catégorie");
    private JButton modifSalle = new JButton("Salle");
    private JButton ajoutSalle = new JButton("ajouter une Salle");
    private JButton supprSalle = new JButton("supprimer une Salle");
    private JTextField nomFilmBd = new JTextField();
    private JTextField dureeFilmBd = new JTextField();
    private JButton validerAjoutFilmBd = new JButton("ajouter");
    private JComboBox boxAnnee = new JComboBox();
    private JComboBox boxMois = new JComboBox();
    private JComboBox boxJour = new JComboBox();
    private JButton validerDateGen = new JButton("valider");
    private JButton optionAjouterFilm = new JButton("ajouter un film");
    private JButton optionSupprimerFilm = new JButton("supprimer un film");
    private JButton validerSuppressionFilmBd = new JButton("supprimer");
    private JButton optionAjouterCategorie = new JButton("ajouter une catégorie");
    private JButton optionSupprimerCategorie = new JButton("supprimer une catégorie");
    private JTextField nomCatBd = new JTextField();
    private JButton validerAjoutCatBd = new JButton("ajouter");
    private JComboBox boxJrd = new JComboBox();
    private JComboBox boxJrf = new JComboBox();
    private JTextField typeCreneau = new JTextField();
    private JButton validerSuppressionCategorieBd = new JButton("supprimer");
    private JButton optionAjouterSalle = new JButton("ajouter une salle");
    private JButton optionSupprimerSalle = new JButton("supprimer une salle");
    private JButton validerAjoutSalleBd = new JButton("ajouter");
    private JTextField nomSalleBd = new JTextField();
    private JButton validerSuppressionSalleBd = new JButton("supprimer");
    private JButton ajouterNewFilm = new JButton("créer un nouveau film");
    private JButton ajouterExistFilm = new JButton("ajouter un film existant");
    private JButton validerAjouterExistFilm = new JButton("ajouter");
    
    ImageIcon image = new ImageIcon("image_festival.png");

    
    public Form() throws SQLException, IOException {
        
        super("Planning");
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);        
        this.setSize(600, 400);
        this.setLocationRelativeTo(null);    
        
        
        DataSource dataSource =  MonMariaDbDataSource.getMdbDataSource();
        Connection connection = dataSource.getConnection();
        
        bd.setConnection(connection);
        
        connection.close();
        
        
        boite.setSize(500, 400);
        boite.setLocationRelativeTo(null);
        boite.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        
        boiteContent.setLayout(new BoxLayout(boiteContent, BoxLayout.Y_AXIS));
        
        dialogueBd.setSize(500, 400);
        dialogueBd.setLocationRelativeTo(null);
        dialogueBd.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        
        boiteBd.setLayout(new BoxLayout(boiteBd, BoxLayout.Y_AXIS));
        
        boiteContent.setBorder(new EmptyBorder(20, 20, 20, 20));

        splitPainGauche.setResizeWeight( 0.1 );

        contentPane.add( splitPainGauche );
        contentPane.add( createMenuBar(), BorderLayout.NORTH );
        
        generer.addActionListener((e) -> {
            dialogueDate();
        });
        
        
        validerDateGen.addActionListener((e) -> {
            Calendar date = Calendar.getInstance();
            Format formatJour = new SimpleDateFormat("d");
            Format formatMois = new SimpleDateFormat("M");
            Format formatAnnee = new SimpleDateFormat("y");
            int jourN = Integer.parseInt(formatJour.format(date.getTime()));
            int moisN = Integer.parseInt(formatMois.format(date.getTime()));
            int anneeN = Integer.parseInt(formatAnnee.format(date.getTime()));
            
            int jour = Integer.parseInt((String) boxJour.getSelectedItem());
            int mois = Integer.parseInt((String) boxMois.getSelectedItem());
            int annee = Integer.parseInt((String) boxAnnee.getSelectedItem());
            
            if (anneeN > annee || anneeN == annee && moisN > mois || anneeN == annee && moisN == mois && jourN > jour)
            {
                JOptionPane.showMessageDialog(null, "Vous avez choisi une date passée !", "Erreur", JOptionPane.ERROR_MESSAGE);
            }
            else
            {
                String dateGen = (String) boxJour.getSelectedItem() + "/" + (String) boxMois.getSelectedItem() + "/" + (String) boxAnnee.getSelectedItem();
                try {
                    GenerationPlanning.generer(dateGen);
                } catch (SQLException ex) {
                    Logger.getLogger(Form.class.getName()).log(Level.SEVERE, null, ex);
                }
                createProg();
                ajoutModif();
                generer.setText("Reset");
                boite.setVisible(false);
                }
            
        });

        ajouter.addActionListener((e) -> {
            dialogueOperationAjouter();
        });
        
        ajouterExistFilm.addActionListener((e) -> {
            try {
                dialogueAjoutExistFilm();
            } catch (SQLException ex) {
                Logger.getLogger(Form.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        
        validerAjouterExistFilm.addActionListener((e) -> {
            try {
                ajoutFilmExist();
            } catch (SQLException ex) {
                Logger.getLogger(Form.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        
        ajouterNewFilm.addActionListener((e) -> {
            try {
                dialogueAjouter();
            } catch (SQLException ex) {
                Logger.getLogger(Form.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
                        
        validAjouter.addActionListener((e) -> {
            try {
                ajoutFilm();
            } catch (SQLException ex) {
                Logger.getLogger(Form.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        
        supprimer.addActionListener((e) -> {
            dialogueSupprimer();
        });
            
        modifSeance.addActionListener((e) -> {
            dialogueEchange();
        });
        
        modifierBd.addActionListener((e) -> {
            dialogueBd();
        });
        
        modifFilm.addActionListener((e) -> {
            dialogueOperationFilm();
            
        });
        
        optionAjouterFilm.addActionListener((e) -> {
            try {
                dialogueAjoutFilmBd();
            } catch (SQLException ex) {
                Logger.getLogger(Form.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        
        optionSupprimerFilm.addActionListener((e) -> {
            try {
                dialogueSupprimerFilmBd();
            } catch (SQLException ex) {
                Logger.getLogger(Form.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
            
        
        validerAjoutFilmBd.addActionListener((e) -> {
            try {
                ajoutFilmBd();
            } catch (SQLException ex) {
                Logger.getLogger(Form.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        
        validerSuppressionFilmBd.addActionListener((e) -> {
            try {
                suppressionFilmBd();
            } catch (SQLException ex) {
                Logger.getLogger(Form.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        
        modifCategorie.addActionListener((e) -> {
            dialogueOperationCategorie();
        });
        
        optionAjouterCategorie.addActionListener((e) -> {
            try {
                dialogueAjoutCatBd();
            } catch (SQLException ex) {
                Logger.getLogger(Form.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        
        validerAjoutCatBd.addActionListener((e) -> {
            try {
                ajoutCategorieBd();
            } catch (SQLException ex) {
                Logger.getLogger(Form.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        
        optionSupprimerCategorie.addActionListener((e) -> {
            try {
                dialogueSupprimerCategorieBd();
            } catch (SQLException ex) {
                Logger.getLogger(Form.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        
        validerSuppressionCategorieBd.addActionListener((e) -> {
            try {           
                suppressionCategorieBd();
            } catch (SQLException ex) {
                Logger.getLogger(Form.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        
        modifSalle.addActionListener((e) -> {
            dialogueOperationSalle();
        });
        
        optionAjouterSalle.addActionListener((e) -> {
            try {
                dialogueAjoutSalleBd();
            } catch (SQLException ex) {
                Logger.getLogger(Form.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        
        validerAjoutSalleBd.addActionListener((e) -> {
            try {
                ajoutSalleBd();
            } catch (SQLException ex) {
                Logger.getLogger(Form.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        
        optionSupprimerSalle.addActionListener((e) -> {
            try {
                dialogueSupprimerSalleBd();
            } catch (SQLException ex) {
                Logger.getLogger(Form.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        
        validerSuppressionSalleBd.addActionListener((e) -> {
            try {
                suppressionSalleBd();
            } catch (SQLException ex) {
                Logger.getLogger(Form.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        
    }
    
    private void ajoutFilmExist() throws SQLException
    {
        
        Statement stmt = null;
        Film film = GenerationPlanning.getFilm((String) boxFilm.getSelectedItem());

        
        Salle salle = GenerationPlanning.getSalle((String) boxSalle.getSelectedItem());
        int heure  = Integer.parseInt((String) boxHeure.getSelectedItem()) * 60 + Integer.parseInt((String) boxMinute.getSelectedItem()); 
        
        boolean test = GenerationPlanning.ajoutCreneau(salle, (String) boxdate.getSelectedItem(), film, heure);
        if (test == true)
        {
            JOptionPane.showMessageDialog(null, "Film ajouté", "Ajout", JOptionPane.INFORMATION_MESSAGE);
        }
        if (test == false)
        {
            JOptionPane.showMessageDialog(null, "Le Film n'a pas pu être ajouté", "Ajout", JOptionPane.ERROR_MESSAGE);
        }
        createProg();  
        textNomFilm.setText("");
        textNomAuteur.setText("");
    
    }
    
    private void dialogueAjoutExistFilm() throws SQLException
    {
        boite.setTitle("Ajouter un film au planning");
        boite.setLocationRelativeTo(null);
        boite.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        boiteContent.setLayout(new BoxLayout(boiteContent, BoxLayout.Y_AXIS));
        
        if (validerSalle.getActionListeners().length != 0)
        {
            validerSalle.removeActionListener(validerSalle.getActionListeners()[0]);
        }
        
        if (validerDate.getActionListeners().length != 0)
        {
            validerDate.removeActionListener(validerDate.getActionListeners()[0]);
        }
        
        if (validerFilm.getActionListeners().length != 0)
        {
            validerFilm.removeActionListener(validerFilm.getActionListeners()[0]);
        }
        
        if (validerHeure.getActionListeners().length != 0)
        {
            validerHeure.removeActionListener(validerHeure.getActionListeners()[0]);
        }
        
        if (validerSuppr.getActionListeners().length != 0)
        {
            validerSuppr.removeActionListener(validerSuppr.getActionListeners()[0]);
        }
        
        if (validerSeance1.getActionListeners().length != 0)
        {
            validerSeance1.removeActionListener(validerSeance1.getActionListeners()[0]);
        }
        
        if (echanger.getActionListeners().length != 0)
        {
            echanger.removeActionListener(echanger.getActionListeners()[0]);
        }
        
        boiteContent.removeAll();
        panelSalle.removeAll();
        paneldate.removeAll();
        panelFilm.removeAll();
        panelHeure.removeAll();
        nomFilmBd.removeAll();
        dureeFilmBd.removeAll();
        
        
        JLabel labelSalle = new JLabel("salle : ");
        boxSalle = getSalles();
        JPanel panelSalle = new JPanel();
        panelSalle.setLayout(new FlowLayout());
        panelSalle.add(labelSalle);
        panelSalle.add(boxSalle);
        boiteContent.add(panelSalle);
        
        // choix de la date
        
        JLabel labeldate = new JLabel("date : ");
        boxdate = getDate((String) boxSalle.getSelectedItem());
        JPanel paneldate = new JPanel();
        paneldate.setLayout(new FlowLayout());
        paneldate.add(labeldate);
        paneldate.add(boxdate);
        boiteContent.add(paneldate);
        
        // choix de l'heure
        
        JLabel labelHeure = new JLabel("heure  : ");
        boxHeure = getHeure();
        boxMinute = getMinute();       
        paneldate.setLayout(new FlowLayout());
        paneldate.add(labelHeure);
        paneldate.add(boxHeure);
        paneldate.add(new JLabel(" h "));
        paneldate.add(boxMinute);
        boiteContent.add(paneldate);

        // choix du nom de Film
        
        JLabel labelNomFilm = new JLabel("Film : ");        
        boxFilm = getAllFilm();
        JPanel panelNomFilm = new JPanel();
        panelNomFilm.setLayout(new FlowLayout());
        panelNomFilm.add(labelNomFilm);
        panelNomFilm.add(boxFilm);
        boiteContent.add(panelNomFilm);
               
        // bouton de validation
        
        JPanel bouttonPanel = new JPanel();
        bouttonPanel.setLayout(new FlowLayout());
        bouttonPanel.add(validerAjouterExistFilm);
        boiteContent.add(bouttonPanel);
        
        validAjouter.addActionListener((e) -> boite.dispose());
        
        boite.setVisible(true);
    }
    
    private void dialogueOperationAjouter()
    {
        boite.setTitle("Ajouter un film au planning");
        boite.setLocationRelativeTo(null);
        boite.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        boiteContent.setLayout(new BoxLayout(boiteContent, BoxLayout.Y_AXIS));
        
        if (validerSalle.getActionListeners().length != 0)
        {
            validerSalle.removeActionListener(validerSalle.getActionListeners()[0]);
        }
        
        if (validerDate.getActionListeners().length != 0)
        {
            validerDate.removeActionListener(validerDate.getActionListeners()[0]);
        }
        
        if (validerFilm.getActionListeners().length != 0)
        {
            validerFilm.removeActionListener(validerFilm.getActionListeners()[0]);
        }
        
        if (validerHeure.getActionListeners().length != 0)
        {
            validerHeure.removeActionListener(validerHeure.getActionListeners()[0]);
        }
        
        if (validerSuppr.getActionListeners().length != 0)
        {
            validerSuppr.removeActionListener(validerSuppr.getActionListeners()[0]);
        }
        
        if (validerSeance1.getActionListeners().length != 0)
        {
            validerSeance1.removeActionListener(validerSeance1.getActionListeners()[0]);
        }
        
        if (echanger.getActionListeners().length != 0)
        {
            echanger.removeActionListener(echanger.getActionListeners()[0]);
        }
        
        boiteContent.removeAll();
        panelSalle.removeAll();
        paneldate.removeAll();
        panelFilm.removeAll();
        panelHeure.removeAll();
        nomFilmBd.removeAll();
        dureeFilmBd.removeAll();
        
        JLabel labelHeader = new JLabel("Opération : ");
        JPanel panelHeader = new JPanel();
        panelHeader.setLayout(new FlowLayout());
        panelHeader.add(labelHeader);
        
        JPanel panelExist = new JPanel();
        panelExist.setLayout(new FlowLayout());
        panelExist.add(ajouterExistFilm);
        
        JPanel panelNew = new JPanel();
        panelNew.setLayout(new FlowLayout());
        panelNew.add(ajouterNewFilm);
        
        boiteContent.add(panelHeader);
        boiteContent.add(panelExist);
        boiteContent.add(panelNew);
        
        boite.setVisible(true);
    
    }
    
    private void suppressionSalleBd() throws SQLException
    {
        DataSource dataSource =  MonMariaDbDataSource.getMdbDataSource();
        Connection connection = dataSource.getConnection();
        
        Statement stmt = null;
        String query = "delete from Salle where nomSalle = \"" + (String) boxSalle.getSelectedItem() + "\"";
        stmt = connection.createStatement();
        boolean test = stmt.execute(query);
        
        if (test == false)
        {
            JOptionPane.showMessageDialog(null, "Salle supprimé", "Suppression", JOptionPane.INFORMATION_MESSAGE);
        }
        if (test == true)
        {
            JOptionPane.showMessageDialog(null, "La salle n'a pas était supprimée", "Suppression", JOptionPane.ERROR_MESSAGE);
        }
        boite.setVisible(false);
        bd.setConnection(connection);
        connection.close();
        
    }
    
    private void dialogueSupprimerSalleBd() throws SQLException
    {
        boite.setTitle("Supprimer une salle de la base de donnée");
        boite.setLocationRelativeTo(null);
        boite.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        boiteContent.setLayout(new BoxLayout(boiteContent, BoxLayout.Y_AXIS));
        
        if (validerSalle.getActionListeners().length != 0)
        {
            validerSalle.removeActionListener(validerSalle.getActionListeners()[0]);
        }
        
        if (validerDate.getActionListeners().length != 0)
        {
            validerDate.removeActionListener(validerDate.getActionListeners()[0]);
        }
        
        if (validerFilm.getActionListeners().length != 0)
        {
            validerFilm.removeActionListener(validerFilm.getActionListeners()[0]);
        }
        
        if (validerHeure.getActionListeners().length != 0)
        {
            validerHeure.removeActionListener(validerHeure.getActionListeners()[0]);
        }
        
        if (validerSuppr.getActionListeners().length != 0)
        {
            validerSuppr.removeActionListener(validerSuppr.getActionListeners()[0]);
        }
        
        if (validerSeance1.getActionListeners().length != 0)
        {
            validerSeance1.removeActionListener(validerSeance1.getActionListeners()[0]);
        }
        
        if (echanger.getActionListeners().length != 0)
        {
            echanger.removeActionListener(echanger.getActionListeners()[0]);
        }
        
        boiteContent.removeAll();
        panelSalle.removeAll();
        paneldate.removeAll();
        panelFilm.removeAll();
        panelHeure.removeAll();
        nomFilmBd.removeAll();
        dureeFilmBd.removeAll();
        
        JLabel labelHeader = new JLabel("Quelle salle voulez vous supprimer ?");
        JPanel panelHeader = new JPanel();
        panelHeader.setLayout(new FlowLayout());
        panelHeader.add(labelHeader);
        
        JPanel panelSalle = new JPanel();
        panelSalle.setLayout(new FlowLayout());
        JLabel labelSalle = new JLabel("Salle : ");
        boxSalle = getAllSalle();
        panelSalle.add(labelSalle);
        panelSalle.add(boxSalle);
        
        boiteContent.add(panelHeader);
        boiteContent.add(panelSalle);
        
        JPanel panelValider = new JPanel();
        panelValider.setLayout(new FlowLayout());
        panelValider.add(validerSuppressionSalleBd);
        
        boiteContent.add(panelValider);
        
        boite.setVisible(true);
    }
    
    private JComboBox getAllSalle() throws SQLException
    {
        DataSource dataSource =  MonMariaDbDataSource.getMdbDataSource();
        Connection connection = dataSource.getConnection();
        
        ResultSet rset = null;
        Statement stmt = null;
        String query = "select nomSalle from Salle";
        stmt = connection.createStatement();
        rset = stmt.executeQuery(query);
        List<String> liste = new ArrayList<>();
        while (rset.next())
        {
            if (rset.getString(1) != null)
            {
                if (!rset.getString(1).equals("null"))
                {
                    liste.add(rset.getString(1));
                }   
            }
        }
        String[] tab = new String[liste.size()];
        for (int i = 0; i < liste.size(); i++)
        {
            tab[i] = liste.get(i);
        }
        JComboBox box = new JComboBox(tab);
        return box;
    }
    
    private void ajoutSalleBd() throws SQLException
    {
        DataSource dataSource =  MonMariaDbDataSource.getMdbDataSource();
        Connection connection = dataSource.getConnection();
        
        Statement stmt = null;
        ResultSet rset = null;
        String query = "select count(*) from Salle";
        stmt = connection.createStatement();
        rset = stmt.executeQuery(query);
        rset.next();
        int id = rset.getInt(1);
        
        int idCat;
        if (((String)boxCategorie.getSelectedItem()).equals("null"))
        {
            idCat = 0;
        }
        else
        {
            query = "select idCat from Categorie where nomCat = \"" + (String) boxCategorie.getSelectedItem() + "\"";
        
            rset = stmt.executeQuery(query);
            rset.next();
            idCat = rset.getInt(1);
        }
     
        int idCat2;
        if (((String)boxCategorie2.getSelectedItem()).equals("null"))
        {
            idCat2 = 0;
        }
        else
        {
            query = "select idCat from Categorie where nomCat = \"" + (String) boxCategorie2.getSelectedItem() + "\"";
        
            rset = stmt.executeQuery(query);
            rset.next();
            idCat2 = rset.getInt(1);
        }
        
              
        query = "insert into Salle values (" + id + ", '" + nomSalleBd.getText() + "', " + idCat + ", " + idCat2 + ")";
        boolean test = stmt.execute(query);
        if (test == false)
        {
            JOptionPane.showMessageDialog(null, "Salle ajouté", "Ajout", JOptionPane.INFORMATION_MESSAGE);
        }
        if (test == true)
        {
            JOptionPane.showMessageDialog(null, "La salle n'a pas été ajoutée", "Ajout", JOptionPane.ERROR_MESSAGE);
        }
        boite.setVisible(false);
        bd.setConnection(connection);
        connection.close();
    }
    
    private void dialogueAjoutSalleBd() throws SQLException
    {
        boite.setTitle("Ajouter une salle à la base de donnée");
        boite.setLocationRelativeTo(null);
        boite.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        boiteContent.setLayout(new BoxLayout(boiteContent, BoxLayout.Y_AXIS));
        
        if (validerSalle.getActionListeners().length != 0)
        {
            validerSalle.removeActionListener(validerSalle.getActionListeners()[0]);
        }
        
        if (validerDate.getActionListeners().length != 0)
        {
            validerDate.removeActionListener(validerDate.getActionListeners()[0]);
        }
        
        if (validerFilm.getActionListeners().length != 0)
        {
            validerFilm.removeActionListener(validerFilm.getActionListeners()[0]);
        }
        
        if (validerHeure.getActionListeners().length != 0)
        {
            validerHeure.removeActionListener(validerHeure.getActionListeners()[0]);
        }
        
        if (validerSuppr.getActionListeners().length != 0)
        {
            validerSuppr.removeActionListener(validerSuppr.getActionListeners()[0]);
        }
        
        if (validerSeance1.getActionListeners().length != 0)
        {
            validerSeance1.removeActionListener(validerSeance1.getActionListeners()[0]);
        }
        
        if (echanger.getActionListeners().length != 0)
        {
            echanger.removeActionListener(echanger.getActionListeners()[0]);
        }
        
        boiteContent.removeAll();
        
        panelSalle.removeAll();
        paneldate.removeAll();
        panelFilm.removeAll();
        panelHeure.removeAll();
        nomFilmBd.removeAll();
        dureeFilmBd.removeAll();
        nomSalleBd.setText("");
        
        
        JLabel labelNom = new JLabel("nom : ");
        nomSalleBd.setPreferredSize(new Dimension(200, 30));
        JPanel panelNom = new JPanel();
        panelNom.setLayout(new FlowLayout());
        panelNom.add(labelNom);
        panelNom.add(nomSalleBd);
        
        boiteContent.add(panelNom);        
        
        JLabel labelCat1 = new JLabel("Catégorie 1 : ");
        boxCategorie = getCategorie();
        boxCategorie.addItem("null");
        JPanel panelCat1 = new JPanel();
        panelCat1.setLayout(new FlowLayout());
        panelCat1.add(labelCat1);
        panelCat1.add(boxCategorie);
        
        boiteContent.add(panelCat1);
        
        JLabel labelCat2 = new JLabel("Catégorie 2 : ");
        boxCategorie2 = getCategorie();
        boxCategorie2.addItem("null");
        JPanel panelCat2 = new JPanel();
        panelCat2.setLayout(new FlowLayout());
        panelCat2.add(labelCat2);
        panelCat2.add(boxCategorie2);
        
        boiteContent.add(panelCat2);
        
        JPanel panelValider = new JPanel();
        panelValider.setLayout(new FlowLayout());
        panelValider.add(validerAjoutSalleBd);
        
        boiteContent.add(panelValider);
        
        
    }
    
    private void dialogueOperationSalle()
    {
        boite.setTitle("Modifier la table Catégorie");
        boite.setLocationRelativeTo(null);
        boite.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        boiteContent.setLayout(new BoxLayout(boiteContent, BoxLayout.Y_AXIS));
        
        if (validerSalle.getActionListeners().length != 0)
        {
            validerSalle.removeActionListener(validerSalle.getActionListeners()[0]);
        }
        
        if (validerDate.getActionListeners().length != 0)
        {
            validerDate.removeActionListener(validerDate.getActionListeners()[0]);
        }
        
        if (validerFilm.getActionListeners().length != 0)
        {
            validerFilm.removeActionListener(validerFilm.getActionListeners()[0]);
        }
        
        if (validerHeure.getActionListeners().length != 0)
        {
            validerHeure.removeActionListener(validerHeure.getActionListeners()[0]);
        }
        
        if (validerSuppr.getActionListeners().length != 0)
        {
            validerSuppr.removeActionListener(validerSuppr.getActionListeners()[0]);
        }
        
        if (validerSeance1.getActionListeners().length != 0)
        {
            validerSeance1.removeActionListener(validerSeance1.getActionListeners()[0]);
        }
        
        if (echanger.getActionListeners().length != 0)
        {
            echanger.removeActionListener(echanger.getActionListeners()[0]);
        }
        
        boiteContent.removeAll();
        panelSalle.removeAll();
        paneldate.removeAll();
        panelFilm.removeAll();
        panelHeure.removeAll();
        nomFilmBd.removeAll();
        dureeFilmBd.removeAll();
        
        JLabel labelOption = new JLabel("Opération : ");
        JPanel panelHeader = new JPanel();
        panelHeader.setLayout(new FlowLayout());
        panelHeader.add(labelOption);
        
        JPanel panelAjout = new JPanel();
        panelAjout.setLayout(new FlowLayout());
        panelAjout.add(optionAjouterSalle);
        
        JPanel panelSuppr = new JPanel();
        panelSuppr.setLayout(new FlowLayout());
        panelSuppr.add(optionSupprimerSalle);
        
        boiteContent.add(panelHeader);
        boiteContent.add(panelAjout);
        boiteContent.add(panelSuppr);
        
        
        
        boite.setVisible(true);
        
    }
    
    
    private void suppressionCategorieBd() throws SQLException
    {
        DataSource dataSource =  MonMariaDbDataSource.getMdbDataSource();
        Connection connection = dataSource.getConnection();
        
        Statement stmt = null;
        String query = "delete from Categorie where nomCat = \"" + (String) boxCategorie.getSelectedItem() + "\"";
        stmt = connection.createStatement();
        boolean test = stmt.execute(query);
        
        if (test == false)
        {
            JOptionPane.showMessageDialog(null, "Catégorie supprimé", "Suppression", JOptionPane.INFORMATION_MESSAGE);
        }
        if (test == true)
        {
            JOptionPane.showMessageDialog(null, "La catégorie n'a pas était supprimée", "Suppression", JOptionPane.ERROR_MESSAGE);
        }
        boite.setVisible(false);
        bd.setConnection(connection);
        connection.close();
        
    }
    
    private void dialogueSupprimerCategorieBd() throws SQLException
    {
        boite.setTitle("Supprimer une catégorie de la base de donnée");
        boite.setLocationRelativeTo(null);
        boite.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        boiteContent.setLayout(new BoxLayout(boiteContent, BoxLayout.Y_AXIS));
        
        if (validerSalle.getActionListeners().length != 0)
        {
            validerSalle.removeActionListener(validerSalle.getActionListeners()[0]);
        }
        
        if (validerDate.getActionListeners().length != 0)
        {
            validerDate.removeActionListener(validerDate.getActionListeners()[0]);
        }
        
        if (validerFilm.getActionListeners().length != 0)
        {
            validerFilm.removeActionListener(validerFilm.getActionListeners()[0]);
        }
        
        if (validerHeure.getActionListeners().length != 0)
        {
            validerHeure.removeActionListener(validerHeure.getActionListeners()[0]);
        }
        
        if (validerSuppr.getActionListeners().length != 0)
        {
            validerSuppr.removeActionListener(validerSuppr.getActionListeners()[0]);
        }
        
        if (validerSeance1.getActionListeners().length != 0)
        {
            validerSeance1.removeActionListener(validerSeance1.getActionListeners()[0]);
        }
        
        if (echanger.getActionListeners().length != 0)
        {
            echanger.removeActionListener(echanger.getActionListeners()[0]);
        }
        
        boiteContent.removeAll();
        panelSalle.removeAll();
        paneldate.removeAll();
        panelFilm.removeAll();
        panelHeure.removeAll();
        nomFilmBd.removeAll();
        dureeFilmBd.removeAll();
        
        JLabel labelHeader = new JLabel("Quelle catégorie voulez vous supprimer ?");
        JPanel panelHeader = new JPanel();
        panelHeader.setLayout(new FlowLayout());
        panelHeader.add(labelHeader);
        
        JPanel panelCat = new JPanel();
        panelCat.setLayout(new FlowLayout());
        JLabel labelCat = new JLabel("Catégorie : ");
        boxCategorie = getAllCat();
        panelCat.add(labelCat);
        panelCat.add(boxCategorie);
        
        boiteContent.add(panelHeader);
        boiteContent.add(panelCat);
        
        JPanel panelValider = new JPanel();
        panelValider.setLayout(new FlowLayout());
        panelValider.add(validerSuppressionCategorieBd);
        
        boiteContent.add(panelValider);
        
        boite.setVisible(true);
    }
    
    private JComboBox getAllCat() throws SQLException
    {
        DataSource dataSource =  MonMariaDbDataSource.getMdbDataSource();
        Connection connection = dataSource.getConnection();
        
        ResultSet rset = null;
        Statement stmt = null;
        String query = "select nomCat from Categorie";
        stmt = connection.createStatement();
        rset = stmt.executeQuery(query);
        List<String> liste = new ArrayList<>();
        while (rset.next())
        {
            if (!rset.getString(1).equals("null"))
            {
                liste.add(rset.getString(1));
            }           
        }
        String[] tab = new String[liste.size()];
        for (int i = 0; i < liste.size(); i++)
        {
            tab[i] = liste.get(i);
        }
        JComboBox box = new JComboBox(tab);
        return box;
    }
    
    private void ajoutCategorieBd() throws SQLException
    {
        DataSource dataSource =  MonMariaDbDataSource.getMdbDataSource();
        Connection connection = dataSource.getConnection();
        
        Statement stmt = null;
        ResultSet rset = null;
        String query = "select count(*) from Categorie";
        stmt = connection.createStatement();
        rset = stmt.executeQuery(query);
        rset.next();
        int id = rset.getInt(1);
        int jrDebut = Integer.parseInt((String) boxJrd.getSelectedItem());
        int jrFin = Integer.parseInt((String) boxJrf.getSelectedItem());
        
        int idLdm;
        if (((String) boxCategorie.getSelectedItem()).equals("null"))
        {
            idLdm = 0;
        }
        else
        {
            query = "select idCat from Categorie where nomCat = \"" + (String) boxCategorie.getSelectedItem() + "\"";
            rset = stmt.executeQuery(query);
            rset.next();
            idLdm = rset.getInt(1);
        }
        
        
        query = "insert into Categorie values (" + id + ", '" + nomCatBd.getText() + "', " + jrDebut + ", " + jrFin + ", '" + typeCreneau.getText() + "', " + idLdm + ")";
        boolean test = stmt.execute(query);
        if (test == false)
        {
            JOptionPane.showMessageDialog(null, "Catégorie ajouté", "Ajout", JOptionPane.INFORMATION_MESSAGE);
        }
        if (test == true)
        {
            JOptionPane.showMessageDialog(null, "La catégorie n'a pas été ajoutée", "Ajout", JOptionPane.ERROR_MESSAGE);
        }
        boite.setVisible(false);
        bd.setConnection(connection);
        connection.close();
    }
    
    private void dialogueAjoutCatBd() throws SQLException
    {
        boite.setTitle("Ajouter une catégorie à la base de donnée");
        boite.setLocationRelativeTo(null);
        boite.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        boiteContent.setLayout(new BoxLayout(boiteContent, BoxLayout.Y_AXIS));
        
        if (validerSalle.getActionListeners().length != 0)
        {
            validerSalle.removeActionListener(validerSalle.getActionListeners()[0]);
        }
        
        if (validerDate.getActionListeners().length != 0)
        {
            validerDate.removeActionListener(validerDate.getActionListeners()[0]);
        }
        
        if (validerFilm.getActionListeners().length != 0)
        {
            validerFilm.removeActionListener(validerFilm.getActionListeners()[0]);
        }
        
        if (validerHeure.getActionListeners().length != 0)
        {
            validerHeure.removeActionListener(validerHeure.getActionListeners()[0]);
        }
        
        if (validerSuppr.getActionListeners().length != 0)
        {
            validerSuppr.removeActionListener(validerSuppr.getActionListeners()[0]);
        }
        
        if (validerSeance1.getActionListeners().length != 0)
        {
            validerSeance1.removeActionListener(validerSeance1.getActionListeners()[0]);
        }
        
        if (echanger.getActionListeners().length != 0)
        {
            echanger.removeActionListener(echanger.getActionListeners()[0]);
        }
        
        boiteContent.removeAll();
        panelSalle.removeAll();
        paneldate.removeAll();
        panelFilm.removeAll();
        panelHeure.removeAll();
        nomFilmBd.removeAll();
        nomCatBd.setText("");
        typeCreneau.setText("");
        dureeFilmBd.removeAll();
        
        JLabel nomCat = new JLabel("Nom : ");
        JPanel panelCat = new JPanel();
        panelCat.setLayout(new FlowLayout());
        panelCat.add(nomCat);
        nomCatBd.setPreferredSize(new Dimension(200, 30));
        panelCat.add(nomCatBd);
        
        boiteContent.add(panelCat);
        
        JLabel labelJrd = new JLabel("début : ");
        JPanel panelJrd = new JPanel();
        panelJrd.setLayout(new FlowLayout());
        panelJrd.add(labelJrd);
        boxJrd = getBox();
        panelJrd.add(boxJrd);
        
        boiteContent.add(panelJrd);
        
        JLabel labelJrf = new JLabel("fin : ");
        JPanel panelJrf = new JPanel();
        panelJrf.setLayout(new FlowLayout());
        panelJrf.add(labelJrf);
        boxJrf = getBox();
        panelJrf.add(boxJrf);
        
        boiteContent.add(panelJrf);
               
        JLabel labelCreneau = new JLabel("type créneau (ex : 1,2,4 ) : ");
        JPanel panelCreneau = new JPanel();
        panelCreneau.setLayout(new FlowLayout());
        panelCreneau.add(labelCreneau);
        typeCreneau.setPreferredSize(new Dimension(200, 30));
        panelCreneau.add(typeCreneau);
        
        boiteContent.add(panelCreneau);
        
        
        JLabel labelCat = new JLabel("Salle de lendemain : ");
        JPanel panelLdm = new JPanel();
        panelLdm.setLayout(new FlowLayout());
        panelLdm.add(labelCat);
        boxCategorie = getCategorie();
        boxCategorie.addItem("null");
        panelLdm.add(boxCategorie);
        
        boiteContent.add(panelLdm);
        
        JPanel panelValider = new JPanel();
        panelValider.setLayout(new FlowLayout());
        panelValider.add(validerAjoutCatBd);
        
        boiteContent.add(panelValider);
        
        
        boite.setVisible(true);
    }
    
    private JComboBox getBox()
    {
        String[] tab = new String[12];
        for (int i = 0; i < 12; i++)
        {
            tab[i] = String.valueOf(i + 1);
        }
        JComboBox box = new JComboBox(tab);
        return box;
    }
    
    private void dialogueOperationCategorie()
    {
        boite.setTitle("Modifier la table Catégorie");
        boite.setLocationRelativeTo(null);
        boite.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        boiteContent.setLayout(new BoxLayout(boiteContent, BoxLayout.Y_AXIS));
        
        if (validerSalle.getActionListeners().length != 0)
        {
            validerSalle.removeActionListener(validerSalle.getActionListeners()[0]);
        }
        
        if (validerDate.getActionListeners().length != 0)
        {
            validerDate.removeActionListener(validerDate.getActionListeners()[0]);
        }
        
        if (validerFilm.getActionListeners().length != 0)
        {
            validerFilm.removeActionListener(validerFilm.getActionListeners()[0]);
        }
        
        if (validerHeure.getActionListeners().length != 0)
        {
            validerHeure.removeActionListener(validerHeure.getActionListeners()[0]);
        }
        
        if (validerSuppr.getActionListeners().length != 0)
        {
            validerSuppr.removeActionListener(validerSuppr.getActionListeners()[0]);
        }
        
        if (validerSeance1.getActionListeners().length != 0)
        {
            validerSeance1.removeActionListener(validerSeance1.getActionListeners()[0]);
        }
        
        if (echanger.getActionListeners().length != 0)
        {
            echanger.removeActionListener(echanger.getActionListeners()[0]);
        }
        
        boiteContent.removeAll();
        panelSalle.removeAll();
        paneldate.removeAll();
        panelFilm.removeAll();
        panelHeure.removeAll();
        nomFilmBd.removeAll();
        dureeFilmBd.removeAll();
        
        JLabel labelOption = new JLabel("Opération : ");
        JPanel panelHeader = new JPanel();
        panelHeader.setLayout(new FlowLayout());
        panelHeader.add(labelOption);
        
        JPanel panelAjout = new JPanel();
        panelAjout.setLayout(new FlowLayout());
        panelAjout.add(optionAjouterCategorie);
        
        JPanel panelSuppr = new JPanel();
        panelSuppr.setLayout(new FlowLayout());
        panelSuppr.add(optionSupprimerCategorie);
        
        boiteContent.add(panelHeader);
        boiteContent.add(panelAjout);
        boiteContent.add(panelSuppr);
        
        
        
        boite.setVisible(true);
        
    }
    
    private void suppressionFilmBd() throws SQLException
    {
        DataSource dataSource =  MonMariaDbDataSource.getMdbDataSource();
        Connection connection = dataSource.getConnection();
        
        Statement stmt = null;
        String query = "delete from Film where nomFilm = \"" + (String) boxFilm.getSelectedItem() + "\"";
        stmt = connection.createStatement();
        boolean test = stmt.execute(query);
        
        if (test == false)
        {
            JOptionPane.showMessageDialog(null, "Film supprimé", "Suppression", JOptionPane.INFORMATION_MESSAGE);
        }
        if (test == true)
        {
            JOptionPane.showMessageDialog(null, "Le film n'a pas était supprimé", "Suppression", JOptionPane.ERROR_MESSAGE);
        }
        boite.setVisible(false);
        bd.setConnection(connection);
        connection.close();
        
    }
    
    private void dialogueSupprimerFilmBd() throws SQLException
    {
        boite.setTitle("Supprimer un film de la base de donnée");
        boite.setLocationRelativeTo(null);
        boite.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        boiteContent.setLayout(new BoxLayout(boiteContent, BoxLayout.Y_AXIS));
        
        if (validerSalle.getActionListeners().length != 0)
        {
            validerSalle.removeActionListener(validerSalle.getActionListeners()[0]);
        }
        
        if (validerDate.getActionListeners().length != 0)
        {
            validerDate.removeActionListener(validerDate.getActionListeners()[0]);
        }
        
        if (validerFilm.getActionListeners().length != 0)
        {
            validerFilm.removeActionListener(validerFilm.getActionListeners()[0]);
        }
        
        if (validerHeure.getActionListeners().length != 0)
        {
            validerHeure.removeActionListener(validerHeure.getActionListeners()[0]);
        }
        
        if (validerSuppr.getActionListeners().length != 0)
        {
            validerSuppr.removeActionListener(validerSuppr.getActionListeners()[0]);
        }
        
        if (validerSeance1.getActionListeners().length != 0)
        {
            validerSeance1.removeActionListener(validerSeance1.getActionListeners()[0]);
        }
        
        if (echanger.getActionListeners().length != 0)
        {
            echanger.removeActionListener(echanger.getActionListeners()[0]);
        }
        
        boiteContent.removeAll();
        panelSalle.removeAll();
        paneldate.removeAll();
        panelFilm.removeAll();
        panelHeure.removeAll();
        nomFilmBd.removeAll();
        dureeFilmBd.removeAll();
        
        JLabel labelHeader = new JLabel("Quelle film voulez vous supprimer ?");
        JPanel panelHeader = new JPanel();
        panelHeader.setLayout(new FlowLayout());
        panelHeader.add(labelHeader);
        
        JPanel panelFilm = new JPanel();
        panelFilm.setLayout(new FlowLayout());
        JLabel labelFilm = new JLabel("Film : ");
        boxFilm = getAllFilm();
        panelFilm.add(labelFilm);
        panelFilm.add(boxFilm);
        
        boiteContent.add(panelHeader);
        boiteContent.add(panelFilm);
        
        JPanel panelValider = new JPanel();
        panelValider.setLayout(new FlowLayout());
        panelValider.add(validerSuppressionFilmBd);
        
        boiteContent.add(panelValider);
        
        boite.setVisible(true);
    }
    
    private JComboBox getAllFilm() throws SQLException
    {
        DataSource dataSource =  MonMariaDbDataSource.getMdbDataSource();
        Connection connection = dataSource.getConnection();
        
        ResultSet rset = null;
        Statement stmt = null;
        String query = "select nomFilm from Film";
        stmt = connection.createStatement();
        rset = stmt.executeQuery(query);
        List<String> liste = new ArrayList<>();
        while (rset.next())
        {
            liste.add(rset.getString(1));
        }
        String[] tab = new String[liste.size()];
        for (int i = 0; i < liste.size(); i++)
        {
            tab[i] = liste.get(i);
        }
        JComboBox box = new JComboBox(tab);
        return box;
    }
    
    private void dialogueOperationFilm()
    {
        boite.setTitle("Modifier la table film");
        boite.setLocationRelativeTo(null);
        boite.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        boiteContent.setLayout(new BoxLayout(boiteContent, BoxLayout.Y_AXIS));
        
        if (validerSalle.getActionListeners().length != 0)
        {
            validerSalle.removeActionListener(validerSalle.getActionListeners()[0]);
        }
        
        if (validerDate.getActionListeners().length != 0)
        {
            validerDate.removeActionListener(validerDate.getActionListeners()[0]);
        }
        
        if (validerFilm.getActionListeners().length != 0)
        {
            validerFilm.removeActionListener(validerFilm.getActionListeners()[0]);
        }
        
        if (validerHeure.getActionListeners().length != 0)
        {
            validerHeure.removeActionListener(validerHeure.getActionListeners()[0]);
        }
        
        if (validerSuppr.getActionListeners().length != 0)
        {
            validerSuppr.removeActionListener(validerSuppr.getActionListeners()[0]);
        }
        
        if (validerSeance1.getActionListeners().length != 0)
        {
            validerSeance1.removeActionListener(validerSeance1.getActionListeners()[0]);
        }
        
        if (echanger.getActionListeners().length != 0)
        {
            echanger.removeActionListener(echanger.getActionListeners()[0]);
        }
        
        boiteContent.removeAll();
        panelSalle.removeAll();
        paneldate.removeAll();
        panelFilm.removeAll();
        panelHeure.removeAll();
        nomFilmBd.removeAll();
        dureeFilmBd.removeAll();
        
        JLabel labelOption = new JLabel("Opération : ");
        JPanel panelHeader = new JPanel();
        panelHeader.setLayout(new FlowLayout());
        panelHeader.add(labelOption);
        
        JPanel panelAjout = new JPanel();
        panelAjout.setLayout(new FlowLayout());
        panelAjout.add(optionAjouterFilm);
        
        JPanel panelSuppr = new JPanel();
        panelSuppr.setLayout(new FlowLayout());
        panelSuppr.add(optionSupprimerFilm);
        
        boiteContent.add(panelHeader);
        boiteContent.add(panelAjout);
        boiteContent.add(panelSuppr);
        
        
        
        boite.setVisible(true);
        
    }
    
    
    
    private void ajoutFilmBd() throws SQLException
    {
        int idCat = -1;
        int duree = Integer.parseInt(dureeFilmBd.getText());
        String nom = nomFilmBd.getText();
        DataSource dataSource =  MonMariaDbDataSource.getMdbDataSource();
        Connection connection = dataSource.getConnection();
        ResultSet rset = null;
        Statement stmt = null;
        stmt = connection.createStatement();
        String query = "select count(*) from Film";
        rset = stmt.executeQuery(query);
        rset.next();
        int id = rset.getInt(1) + 1;
        query = "select idCat, nomCat from Categorie";
        rset = stmt.executeQuery(query);
        while (rset.next())
        {
            if (rset.getString(2).equals((String) boxCategorie.getSelectedItem()))
            {
                idCat = rset.getInt(1);
            }
        }
        query = "insert into Film values (" + id + ", '" + nom + "', " + duree + ", " + idCat + ")";
        boolean test = stmt.execute(query);
        
        if (test == false)
        {
            JOptionPane.showMessageDialog(null, "Film ajouté", "Ajout", JOptionPane.INFORMATION_MESSAGE);
        }
        if (test == true)
        {
            JOptionPane.showMessageDialog(null, "Le film n'a pas était ajouté", "Ajout", JOptionPane.ERROR_MESSAGE);
        }
        boite.setVisible(false);
        bd.setConnection(connection);
        connection.close();
        
        
    }
    
    private void dialogueAjoutFilmBd() throws SQLException 
    {
        boite.setTitle("Ajouter un film à la base de donnée");
        boite.setLocationRelativeTo(null);
        boite.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        boiteContent.setLayout(new BoxLayout(boiteContent, BoxLayout.Y_AXIS));
        
        if (validerSalle.getActionListeners().length != 0)
        {
            validerSalle.removeActionListener(validerSalle.getActionListeners()[0]);
        }
        
        if (validerDate.getActionListeners().length != 0)
        {
            validerDate.removeActionListener(validerDate.getActionListeners()[0]);
        }
        
        if (validerFilm.getActionListeners().length != 0)
        {
            validerFilm.removeActionListener(validerFilm.getActionListeners()[0]);
        }
        
        if (validerHeure.getActionListeners().length != 0)
        {
            validerHeure.removeActionListener(validerHeure.getActionListeners()[0]);
        }
        
        if (validerSuppr.getActionListeners().length != 0)
        {
            validerSuppr.removeActionListener(validerSuppr.getActionListeners()[0]);
        }
        
        if (validerSeance1.getActionListeners().length != 0)
        {
            validerSeance1.removeActionListener(validerSeance1.getActionListeners()[0]);
        }
        
        if (echanger.getActionListeners().length != 0)
        {
            echanger.removeActionListener(echanger.getActionListeners()[0]);
        }
        
        boiteContent.removeAll();
        panelSalle.removeAll();
        paneldate.removeAll();
        panelFilm.removeAll();
        panelHeure.removeAll();
        nomFilmBd.setText("");
        dureeFilmBd.setText("");
        
        JLabel nomFilm = new JLabel("Nom : ");
        JPanel panelNom = new JPanel();
        panelNom.setLayout(new FlowLayout());
        panelNom.add(nomFilm);
        nomFilmBd.setPreferredSize(new Dimension(200, 30));
        panelNom.add(nomFilmBd);
        
        boiteContent.add(panelNom);
        
        JLabel dureeFilm = new JLabel("Durée : ");
        JPanel panelduree = new JPanel();
        panelduree.setLayout(new FlowLayout());
        panelduree.add(dureeFilm);
        dureeFilmBd.setPreferredSize(new Dimension(50, 30));
        panelduree.add(dureeFilmBd);
        
        boiteContent.add(panelduree);
        
        JLabel categorieFilm = new JLabel("Catégorie : ");
        JPanel panelCategorie = new JPanel();
        panelCategorie.setLayout(new FlowLayout());
        panelCategorie.add(categorieFilm);
        boxCategorie = getCategorie();
        panelCategorie.add(boxCategorie);
        
        boiteContent.add(panelCategorie);
        
        JPanel panelValider = new JPanel();
        panelValider.setLayout(new FlowLayout());
        panelValider.add(validerAjoutFilmBd);
        
        boiteContent.add(panelValider);
        
        
        boite.setVisible(true);
        
    }
    
    private void dialogueBd()
    {
        boiteBd.removeAll();
        boiteBd.setBorder(new EmptyBorder(20, 20, 20, 20));
        dialogueBd.setTitle("Modifier la base de donnée");
        JLabel modifTable = new JLabel("Modifier la table : ");
        
        JPanel panelHeader = new JPanel();
        panelHeader.setLayout(new FlowLayout());
        panelHeader.add(modifTable);
        
        JPanel panelFilm = new JPanel();
        panelFilm.setLayout(new FlowLayout());
        panelFilm.add(modifFilm);
        
        boiteBd.add(panelHeader);
        boiteBd.add(panelFilm);
        
        JPanel panelCategorie = new JPanel();
        panelCategorie.setLayout(new FlowLayout());
        panelCategorie.add(modifCategorie);
        boiteBd.add(panelCategorie);
        
        JPanel panelSalle = new JPanel();
        panelSalle.setLayout(new FlowLayout());
        panelSalle.add(modifSalle);
        boiteBd.add(panelSalle);
        
        dialogueBd.setVisible(true);
    }
    
    private void dialogueEchange()
    {
        boite.setTitle("Séance 1");
        boite.setLocationRelativeTo(null);
        boite.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        boiteContent.setLayout(new BoxLayout(boiteContent, BoxLayout.Y_AXIS));
        
        if (validerSalle.getActionListeners().length != 0)
        {
            validerSalle.removeActionListener(validerSalle.getActionListeners()[0]);
        }
        
        if (validerDate.getActionListeners().length != 0)
        {
            validerDate.removeActionListener(validerDate.getActionListeners()[0]);
        }
        
        if (validerFilm.getActionListeners().length != 0)
        {
            validerFilm.removeActionListener(validerFilm.getActionListeners()[0]);
        }
        
        if (validerHeure.getActionListeners().length != 0)
        {
            validerHeure.removeActionListener(validerHeure.getActionListeners()[0]);
        }
        
        if (validerSuppr.getActionListeners().length != 0)
        {
            validerSuppr.removeActionListener(validerSuppr.getActionListeners()[0]);
        }
        
        if (validerSeance1.getActionListeners().length != 0)
        {
            validerSeance1.removeActionListener(validerSeance1.getActionListeners()[0]);
        }
        
        if (echanger.getActionListeners().length != 0)
        {
            echanger.removeActionListener(echanger.getActionListeners()[0]);
        }
        
        boiteContent.removeAll();
        panelSalle.removeAll();
        paneldate.removeAll();
        panelFilm.removeAll();
        panelHeure.removeAll();

        
        
        
        
        // choix de la Salle
        
        JLabel labelSalle = new JLabel("salle : ");
        boxSalle = getSalles();       
        panelSalle.setLayout(new FlowLayout());
        panelSalle.add(labelSalle);
        panelSalle.add(boxSalle);
        boiteContent.add(panelSalle);
        
        
        
        panelSalle.add(validerSalle);
       
        validerSalle.addActionListener((e) -> {
            JLabel labeldate = new JLabel("date : ");
            boxdate = getDate((String) boxSalle.getSelectedItem());
            paneldate.setLayout(new FlowLayout());
            paneldate.add(labeldate);
            paneldate.add(boxdate);            
            paneldate.add(validerDate); 
            boiteContent.add(paneldate);
            panelSalle.remove(validerSalle);
            
        });
        
        validerDate.addActionListener((e) -> {
            JLabel labelFilm = new JLabel("film : ");
            boxFilm = getFilm((String) boxSalle.getSelectedItem(), (String) boxdate.getSelectedItem());
            
            panelFilm.setLayout(new FlowLayout());
            panelFilm.add(labelFilm);
            panelFilm.add(boxFilm);            
            panelFilm.add(validerFilm);
            boiteContent.add(panelFilm);
            paneldate.remove(validerDate);
           
            
        });
        
        validerFilm.addActionListener((e) -> {
            JLabel labelHeure = new JLabel("heure : ");
            boxHeureFilm = getHeureFilm((String) boxSalle.getSelectedItem(), (String) boxdate.getSelectedItem(), (String) boxFilm.getSelectedItem());
            
            panelHeure.setLayout(new FlowLayout());
            panelHeure.add(labelHeure);
            panelHeure.add(boxHeureFilm);            
            panelHeure.add(validerHeure);
            boiteContent.add(panelHeure);
            panelFilm.remove(validerFilm);
        });
        
        validerHeure.addActionListener((e) -> {
            JPanel panelValider = new JPanel();
            panelValider.setLayout(new FlowLayout());
            panelValider.add(validerSeance1);
            boiteContent.add(panelValider);
            panelHeure.remove(validerHeure);
            
        });
        
        
        validerSeance1.addActionListener((a) -> {
            salleCreneau1 = (String) boxSalle.getSelectedItem();
            dateCreneau1 = (String) boxdate.getSelectedItem();
            filmCreneau1 = (String) boxFilm.getSelectedItem();
            heureCreneau1 = (String) boxHeureFilm.getSelectedItem();
            boiteContent.removeAll();
            panelSalle.removeAll();
            paneldate.removeAll();
            panelFilm.removeAll();
            panelHeure.removeAll();
            
            boiteContent.setLayout(new BoxLayout(boiteContent, BoxLayout.Y_AXIS));
            JPanel header = new JPanel();
            header.setLayout(new FlowLayout());
            JLabel labelFilm1 = new JLabel("Film 1 : ");
            JLabel labelHeaderSalle = new JLabel((String) boxSalle.getSelectedItem());
            JLabel labelHeaderDate = new JLabel((String) boxdate.getSelectedItem());
            JLabel labelHeaderFilm = new JLabel((String) boxFilm.getSelectedItem());
            JLabel labelHeaderHeure = new JLabel((String) boxHeureFilm.getSelectedItem());
            header.add(labelHeaderSalle);
            header.add(labelHeaderDate);
            header.add(labelHeaderFilm);
            header.add(labelHeaderHeure);
            boiteContent.add(header);
            
            boite.setTitle("Séance 2");
            
            if (validerSalle.getActionListeners().length != 0)
            {
            validerSalle.removeActionListener(validerSalle.getActionListeners()[0]);
            }
        
            if (validerDate.getActionListeners().length != 0)
            {
                validerDate.removeActionListener(validerDate.getActionListeners()[0]);
            }

            if (validerFilm.getActionListeners().length != 0)
            {
                validerFilm.removeActionListener(validerFilm.getActionListeners()[0]);
            }

            if (validerHeure.getActionListeners().length != 0)
            {
                validerHeure.removeActionListener(validerHeure.getActionListeners()[0]);
            }

            if (validerSuppr.getActionListeners().length != 0)
            {
                validerSuppr.removeActionListener(validerSuppr.getActionListeners()[0]);
            }

            if (validerSeance1.getActionListeners().length != 0)
            {
                validerSeance1.removeActionListener(validerSeance1.getActionListeners()[0]);
            }







            // choix de la Salle

            JLabel labelSalle2 = new JLabel("salle : ");
            boxSalle = getSalles();       
            panelSalle.setLayout(new FlowLayout());
            panelSalle.add(labelSalle2);
            panelSalle.add(boxSalle);
            boiteContent.add(panelSalle);



            panelSalle.add(validerSalle);

            validerSalle.addActionListener((e) -> {
                JLabel labeldate = new JLabel("date : ");
                boxdate = getDate((String) boxSalle.getSelectedItem());
                paneldate.setLayout(new FlowLayout());
                paneldate.add(labeldate);
                paneldate.add(boxdate);            
                paneldate.add(validerDate); 
                boiteContent.add(paneldate);
                panelSalle.remove(validerSalle);

            });

            validerDate.addActionListener((e) -> {
                JLabel labelFilm = new JLabel("film : ");
                boxFilm = getFilm((String) boxSalle.getSelectedItem(), (String) boxdate.getSelectedItem());

                panelFilm.setLayout(new FlowLayout());
                panelFilm.add(labelFilm);
                panelFilm.add(boxFilm);            
                panelFilm.add(validerFilm);
                boiteContent.add(panelFilm);
                paneldate.remove(validerDate);


            });

            validerFilm.addActionListener((e) -> {
                JLabel labelHeure = new JLabel("heure : ");
                boxHeureFilm = getHeureFilm((String) boxSalle.getSelectedItem(), (String) boxdate.getSelectedItem(), (String) boxFilm.getSelectedItem());

                panelHeure.setLayout(new FlowLayout());
                panelHeure.add(labelHeure);
                panelHeure.add(boxHeureFilm);            
                panelHeure.add(validerHeure);
                boiteContent.add(panelHeure);
                panelFilm.remove(validerFilm);
            });

            validerHeure.addActionListener((e) -> {
                JPanel panelValider = new JPanel();
                panelValider.setLayout(new FlowLayout());
                panelValider.add(echanger);
                boiteContent.add(panelValider);
                panelHeure.remove(validerHeure);
            });

            echanger.addActionListener((e) -> {
                boolean test = echanger(salleCreneau1, dateCreneau1, filmCreneau1, heureCreneau1, (String) boxSalle.getSelectedItem(), (String) boxdate.getSelectedItem(), (String) boxFilm.getSelectedItem(), (String) boxHeureFilm.getSelectedItem());
                createProg();
                boite.setVisible(false);
                if (test = true)
                {
                    JOptionPane.showMessageDialog(null, "Séances échangées", "Échange", JOptionPane.INFORMATION_MESSAGE);
                }
                if (test == false)
                {
                    JOptionPane.showMessageDialog(null, "Les séance ne peuvent pas être échangées", "Échange", JOptionPane.ERROR_MESSAGE);
                }                              
                filmCreneau1 = null;
            });



            });
        
        boite.setVisible(true);
    }
    
    private boolean echanger(String salle1, String date1, String film1, String heure1, String salle2, String date2, String film2, String heure2)
    {
        Journee tmpJournee1 = null;
        int pt1 = -1;
        Salle tmpSalle1 = GenerationPlanning.getSalle(salle1);
        for (int i = 0; i < tmpSalle1.getListJournee().size(); i++)
        {
            if (tmpSalle1.getListJournee().get(i).getDate().equals(date1))
            {
                tmpJournee1 = tmpSalle1.getListJournee().get(i);
            }
        }
        
        for (int i = 0; i < tmpJournee1.getCreneau().size(); i++)
        {
            String tmpHeure = String.valueOf(tmpJournee1.getCreneau().get(i).getHeure() / 60 +" h "+ getMinutes(tmpJournee1.getCreneau().get(i).getHeure()));
            if (tmpJournee1.getCreneau().get(i).getFilm().getNomFilm().equals(film1) && tmpHeure.equals(heure1))
            {
                pt1 = i;
            }
        }
        
        Journee tmpJournee2 = null;
        Salle tmpSalle2 = GenerationPlanning.getSalle(salle2);
        for (int i = 0; i < tmpSalle2.getListJournee().size(); i++)
        {
            if (tmpSalle2.getListJournee().get(i).getDate().equals(date2))
            {
                tmpJournee2 = tmpSalle2.getListJournee().get(i);
            }
        }
        
        for (int i = 0; i < tmpJournee2.getCreneau().size(); i++)
        {
            String tmpHeure = String.valueOf(tmpJournee2.getCreneau().get(i).getHeure() / 60 +" h "+ getMinutes(tmpJournee2.getCreneau().get(i).getHeure()));
            if (tmpJournee2.getCreneau().get(i).getFilm().getNomFilm().equals(film2) && tmpHeure.equals(heure2))
            {
                int x = tmpJournee1.getCreneau().get(pt1).getFilm().getDuree() - tmpJournee2.getCreneau().get(i).getFilm().getDuree();
                if (x < 15 || x > -15)
                {
                    int tmp = tmpJournee2.getCreneau().get(i).getHeure();
                    tmpJournee2.getCreneau().get(i).setHeure(tmpJournee1.getCreneau().get(pt1).getHeure());
                    tmpJournee1.getCreneau().get(pt1).setHeure(tmp);
                    Creneau tmpCreneau = tmpJournee2.getCreneau().get(i);
                    tmpJournee2.getCreneau().set(i, tmpJournee1.getCreneau().get(pt1));
                    tmpJournee1.getCreneau().set(pt1, tmpCreneau);
                    
                    return true;
                    
                }
                
            }
        }
        return false;
        
    }
    
    private Creneau saveFilm(String salle, String date, String film, String heure)
    {
        Journee tmpJournee = null;
        Creneau tmpCreneau = null;
        Salle tmpSalle = GenerationPlanning.getSalle(salle);
        for (int i = 0; i < tmpSalle.getListJournee().size(); i++)
        {
            if (tmpSalle.getListJournee().get(i).getDate().equals(date))
            {
                tmpJournee = tmpSalle.getListJournee().get(i);
            }
        }
        
        for (int i = 0; i < tmpJournee.getCreneau().size(); i++)
        {
            String tmpHeure = String.valueOf(tmpJournee.getCreneau().get(i).getHeure() / 60 +" h "+ getMinutes(tmpJournee.getCreneau().get(i).getHeure()));
            if (tmpJournee.getCreneau().get(i).getFilm().getNomFilm().equals(film) && tmpHeure.equals(heure))
            {
                tmpCreneau = tmpJournee.getCreneau().get(i);
            }
        }
        return tmpCreneau;
    }
    
    private void dialogueSupprimer()
    {
        if (validerSalle.getActionListeners().length != 0)
        {
            validerSalle.removeActionListener(validerSalle.getActionListeners()[0]);
        }
        
        if (validerDate.getActionListeners().length != 0)
        {
            validerDate.removeActionListener(validerDate.getActionListeners()[0]);
        }
        
        if (validerFilm.getActionListeners().length != 0)
        {
            validerFilm.removeActionListener(validerFilm.getActionListeners()[0]);
        }
        
        if (validerHeure.getActionListeners().length != 0)
        {
            validerHeure.removeActionListener(validerHeure.getActionListeners()[0]);
        }
        
        if (validerSuppr.getActionListeners().length != 0)
        {
            validerSuppr.removeActionListener(validerSuppr.getActionListeners()[0]);
        }
        
        boiteContent.removeAll();
        panelSalle.removeAll();
        paneldate.removeAll();
        panelFilm.removeAll();
        panelHeure.removeAll();

        
        
        
        
        // choix de la Salle
        
        JLabel labelSalle = new JLabel("salle : ");
        boxSalle = getSalles();       
        panelSalle.setLayout(new FlowLayout());
        panelSalle.add(labelSalle);
        panelSalle.add(boxSalle);
        boiteContent.add(panelSalle);
        
        
        
        panelSalle.add(validerSalle);
       
        validerSalle.addActionListener((e) -> {
            JLabel labeldate = new JLabel("date : ");
            boxdate = getDate((String) boxSalle.getSelectedItem());
            paneldate.setLayout(new FlowLayout());
            paneldate.add(labeldate);
            paneldate.add(boxdate);            
            paneldate.add(validerDate); 
            boiteContent.add(paneldate);
            panelSalle.remove(validerSalle);
            
        });
        
        validerDate.addActionListener((e) -> {
            JLabel labelFilm = new JLabel("film : ");
            boxFilm = getFilm((String) boxSalle.getSelectedItem(), (String) boxdate.getSelectedItem());
            
            panelFilm.setLayout(new FlowLayout());
            panelFilm.add(labelFilm);
            panelFilm.add(boxFilm);            
            panelFilm.add(validerFilm);
            boiteContent.add(panelFilm);
            paneldate.remove(validerDate);
           
            
        });
        
        validerFilm.addActionListener((e) -> {
            JLabel labelHeure = new JLabel("heure : ");
            boxHeureFilm = getHeureFilm((String) boxSalle.getSelectedItem(), (String) boxdate.getSelectedItem(), (String) boxFilm.getSelectedItem());
            
            panelHeure.setLayout(new FlowLayout());
            panelHeure.add(labelHeure);
            panelHeure.add(boxHeureFilm);            
            panelHeure.add(validerHeure);
            boiteContent.add(panelHeure);
            panelFilm.remove(validerFilm);
        });
        
        validerHeure.addActionListener((e) -> {
            JPanel panelValider = new JPanel();
            panelValider.setLayout(new FlowLayout());
            panelValider.add(validerSuppr);
            boiteContent.add(panelValider);
            panelHeure.remove(validerHeure);
        });
        
        validerSuppr.addActionListener((e) -> {
            boolean test = supprSeance((String) boxSalle.getSelectedItem(), (String) boxdate.getSelectedItem(), (String) boxFilm.getSelectedItem(), (String) boxHeureFilm.getSelectedItem());
            boite.setVisible(false);
            createProg();
            if (test == true)
            {
                JOptionPane.showMessageDialog(null, "Séance supprimer", "Suppression", JOptionPane.INFORMATION_MESSAGE);
            }
            if (test == false)
            {
                JOptionPane.showMessageDialog(null, "Erreur de suppression", "Suppression", JOptionPane.ERROR_MESSAGE);
            }
            
            });

        boite.setVisible(true);
        
        
    }
    
    private boolean supprSeance(String salle, String date, String film, String heure)
    {
        Journee tmpJournee = null;
        Salle tmpSalle = GenerationPlanning.getSalle(salle);
        for (int i = 0; i < tmpSalle.getListJournee().size(); i++)
        {
            if (tmpSalle.getListJournee().get(i).getDate().equals(date))
            {
                tmpJournee = tmpSalle.getListJournee().get(i);
            }
        }
        
        for (int i = 0; i < tmpJournee.getCreneau().size(); i++)
        {
            String tmpHeure = String.valueOf(tmpJournee.getCreneau().get(i).getHeure() / 60 +" h "+ getMinutes(tmpJournee.getCreneau().get(i).getHeure()));
            if (tmpJournee.getCreneau().get(i).getFilm().getNomFilm().equals(film) && tmpHeure.equals(heure))
            {
                tmpJournee.getCreneau().remove(i);
                return true;
            }
        }
        return false;     
    }
    
    private JComboBox getHeureFilm(String salle,String date, String film)
    {
        Journee tmpJournee = null;
        Salle tmpSalle = GenerationPlanning.getSalle(salle);
        for (int i = 0; i < tmpSalle.getListJournee().size(); i++)
        {
            if (tmpSalle.getListJournee().get(i).getDate().equals(date))
            {
                tmpJournee = tmpSalle.getListJournee().get(i);
            }
        }
        List<Integer> liste = new ArrayList<>();
        for (int i = 0; i < tmpJournee.getCreneau().size(); i++)
        {
            if(tmpJournee.getCreneau().get(i).getFilm().getNomFilm().equals(film))
            {
                liste.add(tmpJournee.getCreneau().get(i).getHeure());
            }
        }
        
        String[] tab = new String[liste.size()];
                
        for (int i = 0; i < liste.size(); i++)
        {
            tab[i] = String.valueOf(liste.get(i) / 60 +" h "+ getMinutes(liste.get(i)));
        }
        JComboBox boxHeure = new JComboBox(tab);
        return boxHeure; 
    }
    
    private JComboBox getFilm(String salle, String date)
    {
        
        Journee tmpJournee = null;
        Salle tmpSalle = GenerationPlanning.getSalle(salle);
        for (int i = 0; i < tmpSalle.getListJournee().size(); i++)
        {
            if (tmpSalle.getListJournee().get(i).getDate().equals(date))
            {
                tmpJournee = tmpSalle.getListJournee().get(i);
            }
        }
        List<Film> listeFilm = new ArrayList<>();
        for (int i = 0; i < tmpJournee.getCreneau().size(); i++)
        {
            int cpt = 0;
            for (int j = 0; j < listeFilm.size(); j++)
            {
                if (listeFilm.get(j).getNomFilm().equals(tmpJournee.getCreneau().get(i).getFilm().getNomFilm()))
                {
                    cpt = 1;
                }
            }
            if (cpt == 0  && !tmpJournee.getCreneau().get(i).getFilm().getNomFilm().equals(filmCreneau1))
            {
                listeFilm.add(tmpJournee.getCreneau().get(i).getFilm());
            }
        }
        String[] tab = new String[tmpJournee.getCreneau().size()];
        for (int i = 0; i < listeFilm.size(); i++)
        {
            tab[i] = listeFilm.get(i).getNomFilm();
        }
        JComboBox boxFilm = new JComboBox(tab);
        return boxFilm;  
    }
    
    private void dialogueAjouter() throws SQLException
    {
        JDialog boite = new JDialog(this);
        boite.setSize(500, 400);
        boite.setLocationRelativeTo(null);
        boite.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        JPanel boiteContent = (JPanel) boite.getContentPane();
        boiteContent.setLayout(new BoxLayout(boiteContent, BoxLayout.Y_AXIS));
        JLabel h = new JLabel("h");
        
        textNomFilm.setText("");
        textNomAuteur.setText("");
        
        // choix de la Salle
        
        JLabel labelSalle = new JLabel("salle : ");
        boxSalle = getSalles();
        JPanel panelSalle = new JPanel();
        panelSalle.setLayout(new FlowLayout());
        panelSalle.add(labelSalle);
        panelSalle.add(boxSalle);
        boiteContent.add(panelSalle);
        
        // choix de la date
        
        JLabel labeldate = new JLabel("date : ");
        boxdate = getDate((String) boxSalle.getSelectedItem());
        JPanel paneldate = new JPanel();
        paneldate.setLayout(new FlowLayout());
        paneldate.add(labeldate);
        paneldate.add(boxdate);
        boiteContent.add(paneldate);
        
        // choix de l'heure
        
        JLabel labelHeure = new JLabel("heure  : ");
        boxHeure = getHeure();
        boxMinute = getMinute();       
        paneldate.setLayout(new FlowLayout());
        paneldate.add(labelHeure);
        paneldate.add(boxHeure);
        paneldate.add(h);
        paneldate.add(boxMinute);
        boiteContent.add(paneldate);

        // choix du nom de Film
        
        JLabel labelNomFilm = new JLabel("nom : ");        
        textNomFilm.setPreferredSize(new Dimension(200, 30));
        textNomFilm.setEditable(true);
        JPanel panelNomFilm = new JPanel();
        panelNomFilm.setLayout(new FlowLayout());
        panelNomFilm.add(labelNomFilm);
        panelNomFilm.add(textNomFilm);
        boiteContent.add(panelNomFilm);
        
        // choix de l'auteur
        
        JLabel labelNomAuteur = new JLabel("auteur : ");
        
        textNomAuteur.setPreferredSize(new Dimension(200, 30));
        textNomAuteur.setEditable(true);
        JPanel panelNomAuteur = new JPanel();
        panelNomAuteur.setLayout(new FlowLayout());
        panelNomAuteur.add(labelNomAuteur);
        panelNomAuteur.add(textNomAuteur);
        boiteContent.add(panelNomAuteur);
        
        // choix de la durée
        
        JLabel labelDuree = new JLabel("durée : ");
        
        dureeHeure = getHeure();
        dureeMinute = getMinute();   
        JPanel panelDuree = new JPanel();
        panelDuree.setLayout(new FlowLayout());
        panelDuree.add(labelDuree);
        panelDuree.add(dureeHeure);
        panelDuree.add(h);
        panelDuree.add(dureeMinute);
        boiteContent.add(panelDuree);
        
        // choix de la catégorie
        
        JLabel labelCategorie = new JLabel("catégorie : ");
        boxCategorie = getCategorie();
        JPanel panelCategorie = new JPanel();
        panelCategorie.setLayout(new FlowLayout());
        panelCategorie.add(labelCategorie);
        panelCategorie.add(boxCategorie);
        boiteContent.add(panelCategorie);
        
        // bouton de validation
        
        JPanel bouttonPanel = new JPanel();
        bouttonPanel.setLayout(new FlowLayout());
        bouttonPanel.add(validAjouter);
        boiteContent.add(bouttonPanel);
        
        validAjouter.addActionListener((e) -> boite.dispose());
        
        boite.setVisible(true);
    }
    
    private void dialogueDate()
    {
        if (validerSalle.getActionListeners().length != 0)
        {
            validerSalle.removeActionListener(validerSalle.getActionListeners()[0]);
        }
        
        if (validerDate.getActionListeners().length != 0)
        {
            validerDate.removeActionListener(validerDate.getActionListeners()[0]);
        }
        
        if (validerFilm.getActionListeners().length != 0)
        {
            validerFilm.removeActionListener(validerFilm.getActionListeners()[0]);
        }
        
        if (validerHeure.getActionListeners().length != 0)
        {
            validerHeure.removeActionListener(validerHeure.getActionListeners()[0]);
        }
        
        if (validerSuppr.getActionListeners().length != 0)
        {
            validerSuppr.removeActionListener(validerSuppr.getActionListeners()[0]);
        }
        
        boiteContent.removeAll();
        panelSalle.removeAll();
        paneldate.removeAll();
        panelFilm.removeAll();
        panelHeure.removeAll();
        
        JLabel labelDate = new JLabel("Entrer la date de début : ");
        JPanel panelHeader = new JPanel();
        panelHeader.setLayout(new FlowLayout());
        panelHeader.add(labelDate);
        
        JLabel labelJour = new JLabel("Jour : ");
        boxJour = getBoxJour();
        JLabel labelMois = new JLabel("Mois : ");
        boxMois = getBoxMois();
        JLabel labelAnnee = new JLabel("Année : ");
        boxAnnee = getBoxAnnee();
        
        JPanel panelDate = new JPanel();
        panelDate.setLayout(new FlowLayout());
        panelDate.add(labelJour);
        panelDate.add(boxJour);
        panelDate.add(labelMois);
        panelDate.add(boxMois);
        panelDate.add(labelAnnee);
        panelDate.add(boxAnnee);
        
        boiteContent.add(panelHeader);
        boiteContent.add(panelDate);
        
        JPanel panelValider = new JPanel();
        panelValider.setLayout(new FlowLayout());
        panelValider.add(validerDateGen);
        boiteContent.add(panelValider);
        
        boite.setVisible(true);
        
        
        
        
        
        
        
        
        
    }
    
    public JComboBox getBoxAnnee() 
    {
        String[] tab = new String[20];
        Calendar date = Calendar.getInstance();
        Format format = new SimpleDateFormat("y");
        for (int i = 0; i < 20; i++)
        {
            tab[i] = format.format(date.getTime());
            date.add(Calendar.YEAR, 1);
        }
        JComboBox box = new JComboBox(tab);
        return box;
    }
    
    public JComboBox getBoxMois() 
    {
        String[] tab = new String[12];
        for (int i = 0; i < 12; i++)
        {
            if (i + 1 < 10)
            {
                tab[i] = "0" + String.valueOf(i + 1);
            }
            else
            {
                tab[i] = String.valueOf(i + 1);
            }           
        }
        JComboBox box = new JComboBox(tab);
        return box;
    }
    
    public JComboBox getBoxJour() 
    {
        String[] tab = new String[31];
        for (int i = 0; i < 31; i++)
        {
            if (i + 1 < 10)
            {
                tab[i] = "0" + String.valueOf(i + 1);
            }
            else
            {
                tab[i] = String.valueOf(i + 1);
            }         
        }
        JComboBox box = new JComboBox(tab);
        return box;
    }
    
    private JMenuBar createMenuBar()
    {
        
        menuBar.setLayout(new FlowLayout(FlowLayout.LEFT));
        
        generer.setPreferredSize(new Dimension(150, 30));
           
        menuBar.add(generer);
        
        menuBar.add(modifierBd);
      
        return menuBar;
    }
    
    private static void createProg()
    {
        planning.removeAll();
        
        for (int i = 0; i < GenerationPlanning.getListeSalle().size(); i++)
        {
            JPanel tmpConteneur = new JPanel();
            JScrollPane tmpConteneurScroll = new JScrollPane(tmpConteneur);
            tmpConteneur.setLayout(new BoxLayout(tmpConteneur, BoxLayout.Y_AXIS));
            Salle tmpSalle = GenerationPlanning.getListeSalle().get(i);
            JTextField tmpFieldSalle = new JTextField(tmpSalle.getNomSalle());
            tmpFieldSalle.setBackground(Color.red);
            tmpFieldSalle.setEditable(false);
            tmpConteneur.add(tmpFieldSalle);
            
            for (int j = 0; j < tmpSalle.getListJournee().size(); j++)
            {
                Journee tmpJournee = tmpSalle.getListJournee().get(j);
                if (tmpJournee.getCreneau().size() != 0)
                {
                    JTextField tmpFieldJour = new JTextField(tmpJournee.getDate());
                    tmpFieldJour.setBackground(Color.CYAN);
                    tmpFieldJour.setEditable(false);
                    tmpConteneur.add(tmpFieldJour);
                }                
                
                for (int k = 0; k < tmpJournee.getCreneau().size(); k++)
                {
                    JPanel tmpPanel = new JPanel();
                    tmpPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
                    List<Creneau> listeOrdre = ordre(tmpJournee.getCreneau());
                    Creneau tmpCreneau = listeOrdre.get(k);
                    
                    JTextField tmpFieldFilm = new JTextField(tmpCreneau.getFilm().getNomFilm());
                    tmpFieldFilm.setBackground(Color.ORANGE);
                    tmpFieldFilm.setEditable(false);
                    
                    String heure = String.valueOf(tmpCreneau.getHeure() / 60);
                    String minutes = getMinutes(tmpCreneau.getHeure());
                    JTextField tmpFieldHeure = new JTextField(heure + "h" + minutes);
                    tmpFieldHeure.setEditable(false);
                    
                    tmpPanel.add(tmpFieldHeure);
                    tmpPanel.add(tmpFieldFilm);
                    
                    tmpConteneur.add(tmpPanel);
                   
                }
            }
            planning.addTab(tmpSalle.getNomSalle(), tmpConteneurScroll);
        }
    }
    
    private static String getMinutes(int heure)
    {
        if ((heure % 60) < 10)
        {
            return ("0"+String.valueOf(heure % 60));
        }
        else
        {
            return String.valueOf(heure % 60);
        }
    }
    
    private void ajoutModif()
    { 
        menuBar.add(ajouter);
        menuBar.add(supprimer);
        menuBar.add(modifSeance);      
    }
    
    
    
    private static JComboBox getSalles()
    {       
        String[] tab = new String[GenerationPlanning.getListeSalle().size()];
        for (int i = 0; i < GenerationPlanning.getListeSalle().size(); i++)
        {
            tab[i] =  GenerationPlanning.getListeSalle().get(i).getNomSalle();
        }
        JComboBox boxSalle = new JComboBox(tab);
        return boxSalle;
    }
    
    private static JComboBox getDate(String salle)
    {        
        Salle tmpSalle = GenerationPlanning.getSalle(salle);
        String[] tab = new String[tmpSalle.getListJournee().size()];
        for (int i = 0; i < tmpSalle.getListJournee().size(); i++)
        {
            tab[i] = tmpSalle.getListJournee().get(i).getDate();
        }
        JComboBox boxSalle = new JComboBox(tab);
        return boxSalle;
    }
    
    private void ajoutFilm() throws SQLException
    {
        DataSource dataSource =  MonMariaDbDataSource.getMdbDataSource();
        Connection connection = dataSource.getConnection();
        ResultSet rset = null;
        Statement stmt = null;
        
        int idCat = -1;
        String nom = textNomFilm.getText();
        int duree = Integer.parseInt((String) dureeHeure.getSelectedItem()) * 60 + Integer.parseInt((String) dureeMinute.getSelectedItem());

        
        
        stmt = connection.createStatement();
        String query = "select count(*) from Film";
        rset = stmt.executeQuery(query);
        rset.next();
        int id = rset.getInt(1) + 1;
        
        query = "select idCat, nomCat from Categorie";
        rset = stmt.executeQuery(query);
        
        while (rset.next())
        {
            if (rset.getString(2).equals((String) boxCategorie.getSelectedItem()))
            {
                idCat = rset.getInt(1);
            }
        }

        Film newFilm = new Film(id, textNomFilm.getText(), duree, (String) boxCategorie.getSelectedItem());
        Salle salle = GenerationPlanning.getSalle((String) boxSalle.getSelectedItem());
        int heure  = Integer.parseInt((String) boxHeure.getSelectedItem()) * 60 + Integer.parseInt((String) boxMinute.getSelectedItem()); 
        
        boolean test2 = GenerationPlanning.ajoutCreneau(salle, (String) boxdate.getSelectedItem(), newFilm, heure);
        boolean test1 ;
        if (test2 == true)
        {
            query = "insert into Film values (" + id + ", '" + nom + "', " + duree + ", " + idCat + ")";
            test1 = stmt.execute(query);
            
            if (test1 == false)
            {
                JOptionPane.showMessageDialog(null, "Film ajouté", "Ajout", JOptionPane.INFORMATION_MESSAGE);
            }
        }
        else 
        {
            JOptionPane.showMessageDialog(null, "Le film n'a pas pu être ajouté", "Ajout", JOptionPane.ERROR_MESSAGE);
        }
        
        createProg();  
        textNomFilm.setText("");
        textNomAuteur.setText("");
        boite.setVisible(false);
    }
    
    private static JComboBox getCategorie() throws SQLException
    {
        DataSource dataSource =  MonMariaDbDataSource.getMdbDataSource();
        Connection connection = dataSource.getConnection();
        
        ResultSet rset = null;
        Statement stmt = null;
        String query = "select nomCat from Categorie";
        stmt = connection.createStatement();
        rset = stmt.executeQuery(query);
        List<String> liste = new ArrayList<>();
        while (rset.next())
        {
            if (!rset.getString(1).equals("null"))
            {
                liste.add(rset.getString(1));
            }
            
        }
        connection.close();
        String[] tab = new String[liste.size()];
        for (int i = 0; i < liste.size(); i++)
        {
            tab[i] = liste.get(i);
        }
        
        JComboBox boxCategorie = new JComboBox(tab);
        return boxCategorie;
    }
    
    private static JComboBox getHeure()
    {
        String[] tab = new String[24];
        for (int i = 0; i < 24; i++)
        {
            tab[i] = String.valueOf(i);
        }
        JComboBox boxHeure = new JComboBox(tab);
        return boxHeure;        
    }
    
    private static JComboBox getMinute()
    {
        String[] tab = new String[60];
        for (int i = 0; i < 60; i++)
        {
            tab[i] = String.valueOf(i);
        }
        JComboBox boxMinutes = new JComboBox(tab);
        return boxMinutes;        
    }
    
    private static List<Creneau> ordre(List<Creneau> l)
    {
        Creneau[] tab = new Creneau[l.size()];
        for (int i = 0; i < l.size(); i++)
        {
            tab[i] = l.get(i);
        }

        for (int i = 0; i < tab.length; i++)
        {
            for (int j = i + 1; j < tab.length; j++)
            {
                if (tab[j].getHeure() < tab[i].getHeure())
                {
                    Creneau tmpCreneau = tab[i];
                    tab[i] = tab[j];
                    tab[j] = tmpCreneau;
                }

            }
        }
        
        List<Creneau> retour = new ArrayList<>();
        for (int i = 0; i < tab.length; i++)
        {
            retour.add(tab[i]);
        }
        return retour;
        
    }
            

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 502, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 447, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Form.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Form.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Form.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Form.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
                
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                
                try {
                    new Form().setVisible(true);
                } catch (SQLException ex) {
                    Logger.getLogger(Form.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(Form.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
