/***********************************************************************
 * Module:  Creneau.java
 * Author:  p1913943
 * Purpose: Defines the Class Creneau
 ***********************************************************************/

package Planning;

import java.util.*;

public class Creneau {

    
   private Film film;
   private int heure;
   
   public Creneau(Film film, int heure) {
        this.film = film;
        this.heure = heure;
    }

    public Film getFilm() {
        return film;
    }

    public int getHeure() {
        return heure;
    }

    public void setHeure(int heure) {
        this.heure = heure;
    }
   
    
   

}