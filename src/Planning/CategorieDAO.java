/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Planning;

import java.sql.*;
import java.util.List;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;

/**
 *
 * @author mathiseynaud
 */
public class CategorieDAO {
    
    Connection connexionBD;
    
    public CategorieDAO (Connection conn) {
        this.connexionBD = conn;
    }
    
    public List<Categorie> getCategorie() {
        ResultSet rset = null;
        Statement stmt = null;
        List<Categorie> listeCategorie = null;
        String query = "SELECT Categorie.idCat, Categorie.nomCat, Categorie.jrDebut, Categorie.jrFin, Categorie.typeCreneau, Salle.nomSalle FROM Categorie INNER JOIN Salle ON Categorie.salleLendemain = Salle.idSalle;";
        Categorie art;
        try {           
            stmt = connexionBD.createStatement();
            listeCategorie = new ArrayList<>();
            rset = stmt.executeQuery(query);

            while (rset.next()) {
            art = new Categorie(rset.getInt(1), rset.getString(2), rset.getInt(3), rset.getInt(4), rset.getString(5), rset.getString(6));
            listeCategorie.add(art);
            }

        } catch (SQLException ex) {
            Logger.getLogger( FilmDAO.class.getName() ).log(Level.SEVERE, null, ex);
        } 
    return listeCategorie;
    }
    
}
