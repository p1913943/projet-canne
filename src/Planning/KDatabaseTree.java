/*
 * To change this license header, choose License Headers in Project Properties.
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Planning;

import Planning.KDatabaseTreeNode.UserObject;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.util.EventListener;
import java.util.EventObject;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeCellRenderer;
import javax.swing.tree.TreePath;


public class KDatabaseTree extends JScrollPane {

    private static final long serialVersionUID = 5063144763332643221L;

    private Vector<DatabaseSelectionListener> databaseSelectionListeners = new Vector<DatabaseSelectionListener>();
    private JTree jTree = new JTree();

    
    public KDatabaseTree() {
        super();
        this.setPreferredSize( new Dimension( 300, 200 ) );
        this.setViewportView( jTree );
        jTree.addMouseListener( new MouseAdapter() {
            @Override public void mouseClicked(MouseEvent mouseevent) {
                if ( mouseevent.getClickCount() == 2 ) {        // Double click
                    fireTableSelectionChanged( jTree.getSelectionPath() );
                }
            }
        });
    }


    public void setConnection( Connection connection ) {
        if ( connection == null ) {
            jTree.setModel( null );
            return;
        }
        
        try {
            KDatabaseTreeNode rootNode = new KDatabaseTreeNode( connection );
            jTree.setModel( new DefaultTreeModel( rootNode ) );
            jTree.setCellRenderer( new DatabaseCellRenderer() );
            jTree.expandRow( 1 );
        } catch ( Exception exception ) {
            exception.printStackTrace();
            jTree.setModel( null );
        }
    }

    // --- Ajout d'un renderer pour contrôler les icônes affichées --- 
    
    private static class DatabaseCellRenderer implements TreeCellRenderer {

        private JLabel nodeLabel = new JLabel();

        public Component getTreeCellRendererComponent(JTree tree, Object node, boolean selected, boolean expanded, boolean leaf, int row, boolean hasFocus) {
            UserObject data = (UserObject) ((DefaultMutableTreeNode) node).getUserObject();
            Image image = null;
            try {
                image = ImageIO.read(new File(data.getIconUrl()));
            } catch (IOException ex) {
                Logger.getLogger(KDatabaseTree.class.getName()).log(Level.SEVERE, null, ex);
            }
            Image imageRetaillée = null;
            if (data.getIconUrl().equals("images/tables.jpg"))
            {
                imageRetaillée = image.getScaledInstance(60, 40, Image.SCALE_AREA_AVERAGING);
            }
            
            else
            {
                imageRetaillée = image.getScaledInstance(40, 40, Image.SCALE_AREA_AVERAGING);
            }
            
            
            
            
            
            
            nodeLabel.setIcon( new ImageIcon( imageRetaillée ) );
            nodeLabel.setText( data.getText() );
            return nodeLabel; 
        }
    }

    
    // --- Ajout d'un type de listener et de la classe d'événement associée ---
    
    public static class DatabaseSelectionEvent extends EventObject {

        private static final long serialVersionUID = 6132389637969225980L;
        
        private String tableName = null;
        
        public DatabaseSelectionEvent( Object source, String tableName ) {
            super( source );
            if ( tableName == null ) throw new NullPointerException();
            this.tableName = tableName;
        }
        
        public String getTableName() {
            return this.tableName;
        }
    }
    
    public static interface DatabaseSelectionListener extends EventListener {
        
        public void tableSelectionChanged( DatabaseSelectionEvent event );

    }
    
    public void addDatabaseSelectionListener( DatabaseSelectionListener listener ) {
        this.databaseSelectionListeners.addElement( listener );
    }

    public void removeDatabaseSelectionListener( DatabaseSelectionListener listener ) {
        this.databaseSelectionListeners.remove( listener ); 
    }
    
    private void fireTableSelectionChanged( TreePath path ) {
        Object [] data = path.getPath();
        DefaultMutableTreeNode node = (DefaultMutableTreeNode)data[ data.length-1 ];
        if ( node instanceof KDatabaseTreeNode.TableTreeNode == false ) return;
        
        UserObject userObject = (UserObject) node.getUserObject();
        DatabaseSelectionEvent event = new DatabaseSelectionEvent( this, userObject.getText() );
        for(DatabaseSelectionListener listener : this.databaseSelectionListeners) {
            listener.tableSelectionChanged( event );
        }
    }
        
}